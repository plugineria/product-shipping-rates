import {AddressInterface} from "../../Model/Address/AddressInterface";
import {ExampleShippingAddressNotDefined} from "../../Exception/ExampleShippingAddressNotDefined";

export interface ExampleShippingAddressResolverInterface {
    /**
     * @throws {ExampleShippingAddressNotDefined}
     */
    getExampleShippingAddress(): Promise<AddressInterface>
}
