import {ShippingAddressResolverInterface} from "./ShippingAddressResolverInterface";
import {AddressInterface} from "../../Model/Address/AddressInterface";
import SessionShippingAddressRepositoryInterface from "../../Repository/SessionShippingAddressRepositoryInterface";
import {CustomerShippingAddressResolverInterface} from "./CustomerShippingAddressResolverInterface";
import {CustomerSessionResolverInterface} from "../CustomerSessionResolverInterface";
import {ExampleShippingAddressResolverInterface} from "./ExampleShippingAddressResolverInterface";

export class ShippingAddressResolver implements ShippingAddressResolverInterface {
    constructor(
        private sessionShippingAddressRepository: SessionShippingAddressRepositoryInterface,
        private customerShippingAddressResolver: CustomerShippingAddressResolverInterface,
        private customerSessionResolver: CustomerSessionResolverInterface,
        private exampleShippingAddressResolver: ExampleShippingAddressResolverInterface,
    ) {
    }

    async getDefaultShippingAddress(): Promise<AddressInterface> {
        const sessionAddress = await this.sessionShippingAddressRepository.get();

        if (null !== sessionAddress) {
            return sessionAddress;
        }

        const customerId = await this.customerSessionResolver.getCustomerId();
        const customerShippingAddress = customerId
            ? await this.customerShippingAddressResolver.getPrimaryCustomerShippingAddress(customerId)
            : null;

        if (null === customerShippingAddress) {
            return this.exampleShippingAddressResolver.getExampleShippingAddress();
        }

        await this.sessionShippingAddressRepository.save(customerShippingAddress.address);

        return customerShippingAddress.address;
    }
}
