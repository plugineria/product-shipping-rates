import CustomerShippingAddressRepositoryInterface from "../../Repository/CustomerShippingAddressRepositoryInterface";
import {CustomerShippingAddressIdInterface} from "../../Model/Customer/ShippingAddress/CustomerShippingAddressIdInterface";
import SessionShippingAddressRepositoryInterface from "../../Repository/SessionShippingAddressRepositoryInterface";
import {CustomerSessionResolverInterface} from "../CustomerSessionResolverInterface";
import {CustomerShippingAddressNotFound} from "../../Exception/CustomerShippingAddressNotFound";

export class SessionCustomerShippingAddressUpdater {
    constructor(
        private sessionShippingAddressRepository: SessionShippingAddressRepositoryInterface,
        private customerShippingAddressRepository: CustomerShippingAddressRepositoryInterface,
        private customerSessionResolver: CustomerSessionResolverInterface,
    ) {
    }

    /**
     * @param {CustomerShippingAddressIdInterface} addressId
     * @throws {CustomerShippingAddressNotFound}
     */
    async update(addressId: CustomerShippingAddressIdInterface): Promise<void> {
        const customerAddress = await this.customerShippingAddressRepository.getById(addressId);
        const customerId = await this.customerSessionResolver.getCustomerId();

        if (customerAddress.customerId.toString() !== customerId?.toString()) {
            throw new CustomerShippingAddressNotFound(addressId);
        }

        await this.sessionShippingAddressRepository.save(customerAddress.address);
    }
}