import SessionShippingAddressRepositoryInterface from "../../Repository/SessionShippingAddressRepositoryInterface";
import {PostalCodeAddressResolverInterface} from "../PostalCodeAddressResolverInterface";
import {ExampleShippingAddressResolverInterface} from "./ExampleShippingAddressResolverInterface";
import {PostalCodeAddressNotFound} from "../../Exception/PostalCodeAddressNotFound";

export class SessionPostalCodeAddressUpdater {
    constructor(
        private sessionShippingAddressRepository: SessionShippingAddressRepositoryInterface,
        private postalCodeAddressResolver: PostalCodeAddressResolverInterface,
        private exampleShippingAddressResolver: ExampleShippingAddressResolverInterface,
    ) {
    }

    /**
     * @param {string} postalCode
     * @param {string|null} country
     * @throws {PostalCodeAddressNotFound}
     */
    async update(postalCode: string, country: string|null = null): Promise<void> {
        const postalCodeCountry = country
            ? country
            : (await this.exampleShippingAddressResolver.getExampleShippingAddress()).country;

        const shippingAddress = await this.postalCodeAddressResolver.getAddress(
            postalCode,
            postalCodeCountry,
        );

        if (null === shippingAddress) {
            throw new PostalCodeAddressNotFound(postalCode, country);
        }

        await this.sessionShippingAddressRepository.save(shippingAddress);
    }
}