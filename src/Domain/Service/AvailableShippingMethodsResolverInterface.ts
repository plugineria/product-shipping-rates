import {ShippingMethodInterface} from "../Model/ShippingMethod/ShippingMethodInterface";
import {ProductIdInterface} from "../Model/Product/ProductIdInterface";

export default interface AvailableShippingMethodsResolverInterface {
    getAvailableShippingMethods(productId: ProductIdInterface): Promise<ShippingMethodInterface[]>
}
