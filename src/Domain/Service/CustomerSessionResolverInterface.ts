import {CustomerIdInterface} from "../Model/Customer/CustomerIdInterface";

export interface CustomerSessionResolverInterface {
    getCustomerId(): Promise<CustomerIdInterface|null>
}
