import {AddressInterface} from "../Model/Address/AddressInterface";

export interface PostalCodeAddressResolverInterface {
    getAddress(postalCode: string, country: string): Promise<AddressInterface|null>
}