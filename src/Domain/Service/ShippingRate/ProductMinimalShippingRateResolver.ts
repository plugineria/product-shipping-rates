import ProductMinimalShippingRateResolverInterface from "./ProductMinimalShippingRateResolverInterface";
import {ProductIdInterface} from "../../Model/Product/ProductIdInterface";
import {AddressInterface} from "../../Model/Address/AddressInterface";
import {ShippingRateInterface} from "../../Model/ShippingRate/ShippingRateInterface";
import {ProductShippingRatesResolver} from "./ProductShippingRatesResolver";

export class ProductMinimalShippingRateResolver implements ProductMinimalShippingRateResolverInterface {
    constructor(
        private productShippingRatesResolver: ProductShippingRatesResolver,
    ) {
    }

    async getMinimalShippingRate(
        productId: ProductIdInterface,
        shippingAddress: AddressInterface,
    ): Promise<ShippingRateInterface|null> {
        const shippingRates = await this.productShippingRatesResolver.getProductShippingRates(
            productId,
            shippingAddress,
        );

        if (0 === shippingRates.length) {
            return null;
        }

        return shippingRates.reduce(
            (minimalRate, shippingRate) => {
                return shippingRate.price < minimalRate.price ? shippingRate : minimalRate;
            },
            shippingRates[0],
        );
    }
}