import AvailableShippingMethodsResolverInterface from "../AvailableShippingMethodsResolverInterface";
import {ShippingRateResolverInterface} from "./ShippingRateResolverInterface";
import {ShippingRateInterface} from "../../Model/ShippingRate/ShippingRateInterface";
import {ProductIdInterface} from "../../Model/Product/ProductIdInterface";
import {AddressInterface} from "../../Model/Address/AddressInterface";

export class ProductShippingRatesResolver {
    constructor(
        private availableShippingMethodsResolver: AvailableShippingMethodsResolverInterface,
        private shippingRateResolver: ShippingRateResolverInterface,
    ) {
    }

    async getProductShippingRates(
        productId: ProductIdInterface,
        shippingAddress: AddressInterface
    ): Promise<ShippingRateInterface[]> {
        const shippingMethods = await this.availableShippingMethodsResolver.getAvailableShippingMethods(productId);
        const shippingRatesLists = await Promise.all(shippingMethods.map(
            async shippingMethod => await this.shippingRateResolver.getRates(
                shippingMethod.id,
                productId,
                shippingAddress,
            )
        ));

        return shippingRatesLists.reduce(
            (allRates, methodRates) => allRates.concat(methodRates),
            [],
        );
    }
}