import {ShippingRateInterface} from "../../Model/ShippingRate/ShippingRateInterface";
import {ShippingMethodIdInterface} from "../../Model/ShippingMethod/ShippingMethodIdInterface";
import {ProductIdInterface} from "../../Model/Product/ProductIdInterface";
import {AddressInterface} from "../../Model/Address/AddressInterface";

export interface ShippingRateResolverInterface {
    getRates(
        shippingMethodId: ShippingMethodIdInterface,
        productId: ProductIdInterface,
        shippingAddress: AddressInterface,
    ): Promise<ShippingRateInterface[]>
}