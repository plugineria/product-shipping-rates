import {CustomerShippingAddressIdInterface} from "../Model/Customer/ShippingAddress/CustomerShippingAddressIdInterface";
import {CustomerShippingAddressInterface} from "../Model/Customer/ShippingAddress/CustomerShippingAddressInterface";
import {CustomerIdInterface} from "../Model/Customer/CustomerIdInterface";
import {CustomerShippingAddressNotFound} from "../Exception/CustomerShippingAddressNotFound";

export default interface CustomerShippingAddressRepositoryInterface {
    findById(id: CustomerShippingAddressIdInterface): Promise<CustomerShippingAddressInterface|null>

    /**
     * @param {CustomerShippingAddressIdInterface} id
     * @throws {CustomerShippingAddressNotFound}
     */
    getById(id: CustomerShippingAddressIdInterface): Promise<CustomerShippingAddressInterface>
    findAllByCustomerId(customerId: CustomerIdInterface): Promise<CustomerShippingAddressInterface[]>
}
