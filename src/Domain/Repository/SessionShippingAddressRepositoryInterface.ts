import {AddressInterface} from "../Model/Address/AddressInterface";

export default interface SessionShippingAddressRepositoryInterface {
    get(): Promise<AddressInterface|null>
    save(address: AddressInterface): Promise<void>
}
