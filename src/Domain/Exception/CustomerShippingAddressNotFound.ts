import {CustomerShippingAddressIdInterface} from "../Model/Customer/ShippingAddress/CustomerShippingAddressIdInterface";

export class CustomerShippingAddressNotFound extends Error {
    constructor(readonly id: CustomerShippingAddressIdInterface) {
        super(`Customer shipping address ${id} not found`);

        Object.setPrototypeOf(this, CustomerShippingAddressNotFound.prototype);
    }
}
