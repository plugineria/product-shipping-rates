export class ExampleShippingAddressNotDefined extends Error {
    constructor() {
        super("Example shipping address not defined");

        Object.setPrototypeOf(this, ExampleShippingAddressNotDefined.prototype);
    }
}