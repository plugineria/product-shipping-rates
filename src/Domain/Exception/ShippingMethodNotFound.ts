import {ShippingMethodIdInterface} from "../Model/ShippingMethod/ShippingMethodIdInterface";

export class ShippingMethodNotFound extends Error {
    constructor(readonly id: ShippingMethodIdInterface) {
        super(`Shipping method ${id} not found`);

        Object.setPrototypeOf(this, ShippingMethodNotFound.prototype);
    }
}
