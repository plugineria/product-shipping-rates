export class PostalCodeAddressNotFound extends Error {
    constructor(
        readonly postalCode: string,
        readonly country: string|null,
    ) {
        super(`Address for postal code: ${postalCode} in country ${country} not found`);

        Object.setPrototypeOf(this, PostalCodeAddressNotFound.prototype);
    }
}
