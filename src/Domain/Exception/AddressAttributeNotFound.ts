export class AddressAttributeNotFound extends Error {
    constructor(readonly attributeName: string) {
        super(`Address attribute ${attributeName} not found`);

        Object.setPrototypeOf(this, AddressAttributeNotFound.prototype);
    }
}
