import {ShippingRateFactoryInterface} from "./ShippingRateFactoryInterface";
import {ShippingRate} from "./ShippingRate";
import {ShippingMethodId} from "../ShippingMethod/ShippingMethodId";

export class ShippingRateFactory implements ShippingRateFactoryInterface {
    createFromString(shippingRateJson: string): ShippingRate {
        const shippingRate = JSON.parse(shippingRateJson);

        return new ShippingRate(
            new ShippingMethodId(shippingRate.shippingMethodId),
            shippingRate.code,
            shippingRate.price,
            "title" in shippingRate ? shippingRate.title : null,
            "description" in shippingRate ? shippingRate.description : null,
        );
    }
}