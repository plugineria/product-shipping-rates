import {ShippingRateInterface} from "./ShippingRateInterface";
import {ShippingMethodIdInterface} from "../ShippingMethod/ShippingMethodIdInterface";

export class ShippingRate implements ShippingRateInterface {
    constructor(
        readonly shippingMethodId: ShippingMethodIdInterface,
        readonly code: string,
        readonly price: number,
        readonly title: string|null = null,
        readonly description: string|null = null,
    ) {
    }

    toString(): string {
        return JSON.stringify({
            "shippingMethodId": this.shippingMethodId.toString(),
            "code": this.code,
            "price": this.price,
            "title": this.title,
            "description": this.description,
        });
    }
}