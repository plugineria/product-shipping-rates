import {ShippingRateInterface} from "./ShippingRateInterface";

export interface ShippingRateFactoryInterface {
    createFromString(shippingRateJson: string): ShippingRateInterface
}