import {ShippingMethodIdInterface} from "../ShippingMethod/ShippingMethodIdInterface";

export interface ShippingRateInterface {
    readonly shippingMethodId: ShippingMethodIdInterface
    readonly code: string
    readonly price: number
    readonly title: string|null
    readonly description: string|null

    toString(): string
}