import {CustomerShippingAddressInterface} from "./CustomerShippingAddressInterface";
import {CustomerShippingAddressIdInterface} from "./CustomerShippingAddressIdInterface";
import {CustomerIdInterface} from "../CustomerIdInterface";
import {AddressInterface} from "../../Address/AddressInterface";

export class CustomerShippingAddress implements CustomerShippingAddressInterface {
    constructor(
        public readonly id: CustomerShippingAddressIdInterface,
        public readonly customerId: CustomerIdInterface,
        public readonly address: AddressInterface,
    ) {
    }
}