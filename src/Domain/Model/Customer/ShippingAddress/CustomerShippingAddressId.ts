import {CustomerShippingAddressIdInterface} from "./CustomerShippingAddressIdInterface";

export class CustomerShippingAddressId implements CustomerShippingAddressIdInterface {
    constructor(public readonly id: string) {
    }

    toString(): string {
        return this.id
    }
}