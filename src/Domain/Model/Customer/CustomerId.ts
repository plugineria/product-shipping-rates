import {CustomerIdInterface} from "./CustomerIdInterface";

export class CustomerId implements CustomerIdInterface {
    constructor(
        public readonly id: string
    ) {
    }

    toString(): string {
        return this.id
    }
}