import {AddressFactoryInterface} from "./AddressFactoryInterface";
import {Address} from "./Address";
import {AddressAttributeNotFound} from "../../Exception/AddressAttributeNotFound";

export class AddressFactory implements AddressFactoryInterface {
    createFromString(addressJson: string): Address {
        const address = JSON.parse(addressJson);

        if (address.country === undefined) {
            throw new AddressAttributeNotFound("country");
        }

        if (address.city === undefined) {
            throw new AddressAttributeNotFound("city");
        }

        return new Address(
            address.country,
            address.city,
            "postalCode" in address ? address.postalCode : null,
            "street" in address ? address.street : null,
            "region" in address ? address.region : null,
        );
    }
}
