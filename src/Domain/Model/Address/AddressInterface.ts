export interface AddressInterface {
    readonly country: string;
    readonly city: string;
    readonly postalCode: string | null;
    readonly street: string | null;
    readonly region: string | null;

    equals(address: AddressInterface): boolean;
    toString(): string;
}