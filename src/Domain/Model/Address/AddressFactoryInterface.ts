import {AddressInterface} from "./AddressInterface";

export interface AddressFactoryInterface {
    createFromString(addressJson: string): AddressInterface
}
