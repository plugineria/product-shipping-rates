import {AddressInterface} from "./AddressInterface";

export class Address implements AddressInterface {
    constructor(
        readonly country: string,
        readonly city: string,
        readonly postalCode: string | null = null,
        readonly street: string | null = null,
        readonly region: string | null = null,
    ) {

    }

    equals(address: AddressInterface): boolean {
        return this.toString() === address.toString();
    }

    toString(): string {
        return JSON.stringify({
            "country": this.country,
            "region": this.region,
            "postalCode": this.postalCode,
            "city": this.city,
            "street": this.street,
        });
    }
}