import {ProductIdInterface} from "./ProductIdInterface";

export class ProductId implements ProductIdInterface {
    constructor(readonly id: string) {
    }

    toString(): string {
        return this.id
    }
}