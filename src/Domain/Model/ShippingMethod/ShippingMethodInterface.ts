import {ShippingMethodIdInterface} from "./ShippingMethodIdInterface";

export interface ShippingMethodInterface {
    readonly id: ShippingMethodIdInterface
    readonly title: string
}