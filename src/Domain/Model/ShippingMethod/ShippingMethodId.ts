import {ShippingMethodIdInterface} from "./ShippingMethodIdInterface";

export class ShippingMethodId implements ShippingMethodIdInterface {
    constructor(public readonly id: string) {
    }

    toString(): string {
        return this.id
    }
}