import {ShippingMethodInterface} from "./ShippingMethodInterface";
import {ShippingMethodIdInterface} from "./ShippingMethodIdInterface";

export class ShippingMethod implements ShippingMethodInterface {
    constructor(
        public readonly id: ShippingMethodIdInterface,
        public readonly title: string,
    ) {
    }
}