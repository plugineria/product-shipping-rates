import {ShippingMethodIdInterface} from "../../../Domain/Model/ShippingMethod/ShippingMethodIdInterface";
import {ShippingMethodId} from "../../../Domain/Model/ShippingMethod/ShippingMethodId";
import ShippingMethodTestBuilder from "./ShippingMethodTestBuilder";
import {ShippingRate} from "../../../Domain/Model/ShippingRate/ShippingRate";

export default class ShippingRateTestBuilder {
    static readonly DEFAULT_PRICE = 15.99;
    static readonly DEFAULT_CODE = 'standard';

    private shippingMethodId: ShippingMethodIdInterface;
    private code: string;
    private price: number;
    private title: string|null;
    private description: string|null;

    private constructor() {
        this.shippingMethodId = new ShippingMethodId(ShippingMethodTestBuilder.DEFAULT_ID);
        this.code = ShippingRateTestBuilder.DEFAULT_CODE;
        this.price = ShippingRateTestBuilder.DEFAULT_PRICE;
        this.title = null;
        this.description = null;
    }

    static create(): ShippingRateTestBuilder {
        return new ShippingRateTestBuilder();
    }

    build(): ShippingRate {
        return new ShippingRate(
            this.shippingMethodId,
            this.code,
            this.price,
            this.title,
            this.description,
        );
    }

    withShippingMethodId(shippingMethodId: ShippingMethodIdInterface): ShippingRateTestBuilder {
        this.shippingMethodId = shippingMethodId;

        return this;
    }

    withCode(code: string): ShippingRateTestBuilder {
        this.code = code;

        return this;
    }

    withPrice(price: number): ShippingRateTestBuilder {
        this.price = price;

        return this;
    }

    withTitle(title: string|null): ShippingRateTestBuilder {
        this.title = title;

        return this;
    }

    withDescription(description: string|null): ShippingRateTestBuilder {
        this.description = description;

        return this;
    }
}