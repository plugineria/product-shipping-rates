import {Address} from "../../../Domain/Model/Address/Address";

export default class AddressTestBuilder {
    static readonly COUNTRY = 'PL';
    static readonly CITY = 'Warszawa';

    private constructor(
        private country: string = AddressTestBuilder.COUNTRY,
        private city: string = AddressTestBuilder.CITY,
        private postalCode: string|null = null,
        private region: string|null = null,
        private street: string|null = null,
    ) {
    }

    static create(): AddressTestBuilder {
        return new AddressTestBuilder();
    }

    build(): Address {
        return new Address(
            this.country,
            this.city,
            this.postalCode,
            this.street,
            this.region,
        );
    }

    withCountry(value: string): AddressTestBuilder {
        this.country = value;

        return this;
    }

    withCity(value: string): AddressTestBuilder {
        this.city = value;

        return this;
    }

    withPostalCode(value: string | null): AddressTestBuilder {
        this.postalCode = value;

        return this;
    }

    withRegion(value: string | null): AddressTestBuilder {
        this.region = value;

        return this;
    }

    withStreet(value: string | null): AddressTestBuilder {
        this.street = value;

        return this;
    }
}
