import {ShippingMethod} from "../../../Domain/Model/ShippingMethod/ShippingMethod";
import {ShippingMethodIdInterface} from "../../../Domain/Model/ShippingMethod/ShippingMethodIdInterface";
import {ShippingMethodId} from "../../../Domain/Model/ShippingMethod/ShippingMethodId";

export default class ShippingMethodTestBuilder {
    static readonly DEFAULT_ID: string = 'dpd';
    static readonly DEFAULT_NAME: string = 'DPD';

    private id: ShippingMethodIdInterface;
    private title: string;

    private constructor() {
        this.id = new ShippingMethodId(ShippingMethodTestBuilder.DEFAULT_ID);
        this.title = ShippingMethodTestBuilder.DEFAULT_NAME;
    }

    static create(): ShippingMethodTestBuilder {
        return new ShippingMethodTestBuilder();
    }

    build(): ShippingMethod {
        return new ShippingMethod(
            this.id,
            this.title,
        );
    }

    withId(id: ShippingMethodIdInterface): ShippingMethodTestBuilder {
        this.id = id;

        return this;
    }

    withTitle(title: string): ShippingMethodTestBuilder {
        this.title = title;

        return this;
    }
}