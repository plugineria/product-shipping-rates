import {CustomerShippingAddressIdInterface} from "../../../Domain/Model/Customer/ShippingAddress/CustomerShippingAddressIdInterface";
import {CustomerIdInterface} from "../../../Domain/Model/Customer/CustomerIdInterface";
import {AddressInterface} from "../../../Domain/Model/Address/AddressInterface";
import AddressTestBuilder from "./AddressTestBuilder";
import {CustomerId} from "../../../Domain/Model/Customer/CustomerId";
import * as crypto from "crypto";
import {CustomerShippingAddressId} from "../../../Domain/Model/Customer/ShippingAddress/CustomerShippingAddressId";
import {CustomerShippingAddress} from "../../../Domain/Model/Customer/ShippingAddress/CustomerShippingAddress";

export default class CustomerShippingAddressTestBuilder {
    private id: CustomerShippingAddressIdInterface = new CustomerShippingAddressId(
        `customer_shipping_address_${crypto.randomBytes(20).toString('hex')}`
    );

    private customerId: CustomerIdInterface = new CustomerId(
        `customer_${crypto.randomBytes(20).toString('hex')}`
    );

    private address: AddressInterface = AddressTestBuilder.create().build();

    build(): CustomerShippingAddress {
        return new CustomerShippingAddress(this.id, this.customerId, this.address);
    }

    withId(id: CustomerShippingAddressIdInterface): CustomerShippingAddressTestBuilder {
        this.id = id;

        return this;
    }

    withCustomerId(customerId: CustomerIdInterface): CustomerShippingAddressTestBuilder {
        this.customerId = customerId;

        return this;
    }

    withAddress(address: AddressInterface): CustomerShippingAddressTestBuilder {
        this.address = address;

        return this;
    }
}