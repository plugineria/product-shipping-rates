import ProductMinimalShippingRateQuery from "../../../App/Query/ProductMinimalShippingRateQuery";
import {ProductShippingRatesQuery} from "../../../App/Query/ProductShippingRatesQuery";
import {SessionShippingAddressQuery} from "../../../App/Query/SessionShippingAddressQuery";
import {SetSessionShippingAddressUseCase} from "../../../App/UseCase/SetSessionShippingAddress/SetSessionShippingAddressUseCase";
import {MockedCustomerSessionResolver} from "../../Mock/Service/MockedCustomerSessionResolver";
import {MockedShippingRateResolver} from "../../Mock/Service/ShippingRate/MockedShippingRateResolver";
import {MockedAvailableShippingMethodsResolver} from "../../Mock/Service/MockedAvailableShippingMethodsResolver";
import {MockedCustomerShippingAddressResolver} from "../../Mock/Service/ShippingAddress/MockedCustomerShippingAddressResolver";
import {InMemoryShippingMethodRepository} from "../../InMemory/Repository/InMemoryShippingMethodRepository";
import {InMemorySessionShippingAddressRepository} from "../../InMemory/Repository/InMemorySessionShippingAddressRepository";
import {InMemoryCustomerShippingAddressRepository} from "../../InMemory/Repository/InMemoryCustomerShippingAddressRepository";
import {MockedPostalCodeAddressResolver} from "../../Mock/Service/MockedPostalCodeAddressResolver";
import {MockedExampleShippingAddressResolver} from "../../Mock/Service/ShippingAddress/MockedExampleShippingAddressResolver";
import {ShippingAddressResolver} from "../../../Domain/Service/ShippingAddress/ShippingAddressResolver";
import {ShippingRateViewFactory} from "../../../App/View/ShippingRateViewFactory";
import {ProductMinimalShippingRateResolver} from "../../../Domain/Service/ShippingRate/ProductMinimalShippingRateResolver";
import {ProductShippingRatesResolver} from "../../../Domain/Service/ShippingRate/ProductShippingRatesResolver";
import {SessionCustomerShippingAddressUpdater} from "../../../Domain/Service/ShippingAddress/SessionCustomerShippingAddressUpdater";
import {SessionPostalCodeAddressUpdater} from "../../../Domain/Service/ShippingAddress/SessionPostalCodeAddressUpdater";

export default class TestApp {
    readonly productMinimalShippingRateQuery: ProductMinimalShippingRateQuery;
    readonly productShippingRatesQuery: ProductShippingRatesQuery;
    readonly sessionShippingAddressQuery: SessionShippingAddressQuery;
    readonly setSessionShippingAddressUseCase: SetSessionShippingAddressUseCase;

    readonly customerSessionResolver: MockedCustomerSessionResolver;
    readonly shippingRateResolver: MockedShippingRateResolver;
    readonly availableShippingMethodsResolver: MockedAvailableShippingMethodsResolver;
    readonly customerShippingAddressResolver: MockedCustomerShippingAddressResolver;
    readonly shippingMethodRepository: InMemoryShippingMethodRepository;
    readonly sessionShippingAddressRepository: InMemorySessionShippingAddressRepository;
    readonly customerShippingAddressRepository: InMemoryCustomerShippingAddressRepository;
    readonly postalCodeAddressResolver: MockedPostalCodeAddressResolver;
    readonly exampleShippingAddressResolver: MockedExampleShippingAddressResolver;

    constructor() {
        this.customerSessionResolver = new MockedCustomerSessionResolver();
        this.shippingRateResolver = new MockedShippingRateResolver();
        this.shippingMethodRepository = new InMemoryShippingMethodRepository();
        this.availableShippingMethodsResolver = new MockedAvailableShippingMethodsResolver(
            this.shippingMethodRepository
        );

        this.customerShippingAddressResolver = new MockedCustomerShippingAddressResolver();
        this.sessionShippingAddressRepository = new InMemorySessionShippingAddressRepository();
        this.customerShippingAddressRepository = new InMemoryCustomerShippingAddressRepository();
        this.postalCodeAddressResolver = new MockedPostalCodeAddressResolver();
        this.exampleShippingAddressResolver = new MockedExampleShippingAddressResolver();

        const shippingAddressResolver = new ShippingAddressResolver(
            this.sessionShippingAddressRepository,
            this.customerShippingAddressResolver,
            this.customerSessionResolver,
            this.exampleShippingAddressResolver
        );

        const shippingRateViewFactory = new ShippingRateViewFactory(this.shippingMethodRepository);
        const productShippingRatesResolver = new ProductShippingRatesResolver(
            this.availableShippingMethodsResolver,
            this.shippingRateResolver,
        );

        this.productMinimalShippingRateQuery = new ProductMinimalShippingRateQuery(
            new ProductMinimalShippingRateResolver(
                productShippingRatesResolver,
            ),
            shippingAddressResolver,
            shippingRateViewFactory,
        );

        this.productShippingRatesQuery = new ProductShippingRatesQuery(
            productShippingRatesResolver,
            shippingAddressResolver,
            shippingRateViewFactory,
        );

        this.setSessionShippingAddressUseCase = new SetSessionShippingAddressUseCase(
            new SessionCustomerShippingAddressUpdater(
                this.sessionShippingAddressRepository,
                this.customerShippingAddressRepository,
                this.customerSessionResolver,
            ),
            new SessionPostalCodeAddressUpdater(
                this.sessionShippingAddressRepository,
                this.postalCodeAddressResolver,
                this.exampleShippingAddressResolver,
            )
        );

        this.sessionShippingAddressQuery = new SessionShippingAddressQuery(
            shippingAddressResolver,
            this.customerShippingAddressRepository,
            this.customerSessionResolver,
            this.exampleShippingAddressResolver,
        );
    }
}
