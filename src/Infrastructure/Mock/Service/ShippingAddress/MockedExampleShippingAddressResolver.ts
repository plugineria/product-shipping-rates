import {ExampleShippingAddressResolverInterface} from "../../../../Domain/Service/ShippingAddress/ExampleShippingAddressResolverInterface";
import {AddressInterface} from "../../../../Domain/Model/Address/AddressInterface";
import {ExampleShippingAddressNotDefined} from "../../../../Domain/Exception/ExampleShippingAddressNotDefined";

export class MockedExampleShippingAddressResolver implements ExampleShippingAddressResolverInterface {
    exampleShippingAddress: AddressInterface|null = null;

    async getExampleShippingAddress(): Promise<AddressInterface> {
        if (null === this.exampleShippingAddress) {
            throw new ExampleShippingAddressNotDefined();
        }

        return this.exampleShippingAddress;
    }
}