import {CustomerShippingAddressResolverInterface} from "../../../../Domain/Service/ShippingAddress/CustomerShippingAddressResolverInterface";
import {CustomerIdInterface} from "../../../../Domain/Model/Customer/CustomerIdInterface";
import {CustomerShippingAddressInterface} from "../../../../Domain/Model/Customer/ShippingAddress/CustomerShippingAddressInterface";

export class MockedCustomerShippingAddressResolver implements CustomerShippingAddressResolverInterface {
    constructor(
        private defaultCustomerShippingAddresses: PrimaryCustomerShippingAddressCollectionByCustomerId = {},
    ) {
    }

    async getPrimaryCustomerShippingAddress(
        customerId: CustomerIdInterface
    ): Promise<CustomerShippingAddressInterface|null> {
        return this.defaultCustomerShippingAddresses[customerId.toString()] ?? null;
    }

    setCustomerPrimaryShippingAddress(address: CustomerShippingAddressInterface): void {
        this.defaultCustomerShippingAddresses[address.customerId.toString()] = address;
    }
}

interface PrimaryCustomerShippingAddressCollectionByCustomerId {
    [key: string]: CustomerShippingAddressInterface|null
}