import AvailableShippingMethodsResolverInterface from "../../../Domain/Service/AvailableShippingMethodsResolverInterface";
import {ShippingMethodRepositoryInterface} from "../../../Domain/Repository/ShippingMethodRepositoryInterface";
import {ProductIdInterface} from "../../../Domain/Model/Product/ProductIdInterface";
import {ShippingMethodInterface} from "../../../Domain/Model/ShippingMethod/ShippingMethodInterface";
import {ShippingMethodIdInterface} from "../../../Domain/Model/ShippingMethod/ShippingMethodIdInterface";

export class MockedAvailableShippingMethodsResolver implements AvailableShippingMethodsResolverInterface {
    shippingMethodIds: ShippingMethodIdInterface[] = [];

    constructor(private shippingMethodRepository: ShippingMethodRepositoryInterface) {
    }

    async getAvailableShippingMethods(productId: ProductIdInterface): Promise<ShippingMethodInterface[]> {
        const shippingMethods: ShippingMethodInterface[] = [];

        for (const shippingMethodId of this.shippingMethodIds) {
            const shippingMethod = await this.shippingMethodRepository.findById(shippingMethodId);

            if (null !== shippingMethod) {
                shippingMethods.push(shippingMethod);
            }
        }

        return shippingMethods;
    }
}