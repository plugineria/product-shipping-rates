import {ShippingRateResolverInterface} from "../../../../Domain/Service/ShippingRate/ShippingRateResolverInterface";
import {ShippingMethodIdInterface} from "../../../../Domain/Model/ShippingMethod/ShippingMethodIdInterface";
import {ProductIdInterface} from "../../../../Domain/Model/Product/ProductIdInterface";
import {AddressInterface} from "../../../../Domain/Model/Address/AddressInterface";
import {ShippingRateInterface} from "../../../../Domain/Model/ShippingRate/ShippingRateInterface";

export class MockedShippingRateResolver implements ShippingRateResolverInterface {
    ratesPerShippingMethod: RatesCollectionByShippingMethodId = {};

    async getRates(
        shippingMethodId: ShippingMethodIdInterface,
        productId: ProductIdInterface,
        shippingAddress: AddressInterface,
    ): Promise<ShippingRateInterface[]> {
        return this.ratesPerShippingMethod[shippingMethodId.toString()] ?? [];
    }
}

interface RatesCollectionByShippingMethodId {
    [key: string]: ShippingRateInterface[]
}
