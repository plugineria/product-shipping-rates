import {PostalCodeAddressResolverInterface} from "../../../Domain/Service/PostalCodeAddressResolverInterface";
import {Address} from "../../../Domain/Model/Address/Address";

export class MockedPostalCodeAddressResolver implements PostalCodeAddressResolverInterface {
    city: string|null = null;

    async getAddress(postalCode: string, country: string): Promise<Address|null> {
        if (null === this.city) {
            return null;
        }

        return new Address(
            country,
            this.city,
            postalCode,
        );
    }
}
