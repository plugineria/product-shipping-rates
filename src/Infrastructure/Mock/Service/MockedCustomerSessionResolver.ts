import {CustomerSessionResolverInterface} from "../../../Domain/Service/CustomerSessionResolverInterface";
import {CustomerIdInterface} from "../../../Domain/Model/Customer/CustomerIdInterface";

export class MockedCustomerSessionResolver implements CustomerSessionResolverInterface {
    customerId: CustomerIdInterface|null = null;

    async getCustomerId(): Promise<CustomerIdInterface|null> {
        return this.customerId;
    }
}
