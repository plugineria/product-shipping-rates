import {CacheInterface} from "../../../App/Service/CacheInterface";

export class InMemoryCache implements CacheInterface {
    private storage: InMemoryCacheStorage = {};

    async get(key: string): Promise<string|null> {
        if (!(key in this.storage)) {
            return null;
        }

        const cacheEntity = this.storage[key];
        if (null !== cacheEntity.ttl && cacheEntity.timestamp + cacheEntity.ttl < Date.now()) {
            delete this.storage[key];
            return null;
        }

        return this.storage[key].value;
    }

    async set(key: string, value: string, ttlSec: number|null = null): Promise<void> {
        this.storage[key] = {
            "value": value,
            "ttl": ttlSec,
            "timestamp": Date.now(),
        };
    }

    getAll(): InMemoryCacheStorage {
        return this.storage;
    }
}

interface InMemoryCacheStorage {
    [index: string]: InMemoryCacheEntity
}

interface InMemoryCacheEntity {
    readonly value: string,
    readonly ttl: number|null,
    readonly timestamp: number,
}