import SessionShippingAddressRepositoryInterface from "../../../Domain/Repository/SessionShippingAddressRepositoryInterface";
import {AddressInterface} from "../../../Domain/Model/Address/AddressInterface";

export class InMemorySessionShippingAddressRepository implements SessionShippingAddressRepositoryInterface {
    constructor(private address: AddressInterface|null = null) {
    }

    async get(): Promise<AddressInterface|null> {
        return null === this.address ? null : {...this.address};
    }

    async save(address: AddressInterface): Promise<void> {
        this.address = address;
    }

    reset(): void {
        this.address = null;
    }
}
