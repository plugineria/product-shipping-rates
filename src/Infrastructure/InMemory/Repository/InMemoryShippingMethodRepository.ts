import {ShippingMethodRepositoryInterface} from "../../../Domain/Repository/ShippingMethodRepositoryInterface";
import {ShippingMethodInterface} from "../../../Domain/Model/ShippingMethod/ShippingMethodInterface";
import {ShippingMethodIdInterface} from "../../../Domain/Model/ShippingMethod/ShippingMethodIdInterface";
import {ShippingMethodNotFound} from "../../../Domain/Exception/ShippingMethodNotFound";

export class InMemoryShippingMethodRepository implements ShippingMethodRepositoryInterface {
    private shippingMethods: ShippingMethodCollection = {};

    async findById(id: ShippingMethodIdInterface): Promise<ShippingMethodInterface|null> {
        if (id.toString() in this.shippingMethods) {
            return this.shippingMethods[id.toString()];
        }

        return null;
    }

    async getById(id: ShippingMethodIdInterface): Promise<ShippingMethodInterface> {
        const shippingMethod = await this.findById(id);

        if (null === shippingMethod) {
            throw new ShippingMethodNotFound(id);
        }

        return shippingMethod;
    }

    save(...shippingMethods: ShippingMethodInterface[]): void {
        for (const shippingMethod of shippingMethods) {
            this.shippingMethods[shippingMethod.id.toString()] = shippingMethod;
        }
    }
}

interface ShippingMethodCollection {
    [index: string]: ShippingMethodInterface
}