import CustomerShippingAddressRepositoryInterface from "../../../Domain/Repository/CustomerShippingAddressRepositoryInterface";
import {CustomerShippingAddressIdInterface} from "../../../Domain/Model/Customer/ShippingAddress/CustomerShippingAddressIdInterface";
import {CustomerShippingAddressInterface} from "../../../Domain/Model/Customer/ShippingAddress/CustomerShippingAddressInterface";
import {CustomerIdInterface} from "../../../Domain/Model/Customer/CustomerIdInterface";
import {CustomerShippingAddressNotFound} from "../../../Domain/Exception/CustomerShippingAddressNotFound";

export class InMemoryCustomerShippingAddressRepository implements CustomerShippingAddressRepositoryInterface {
    constructor(private addresses: AddressCollection = {}) {
    }

    async getById(id: CustomerShippingAddressIdInterface): Promise<CustomerShippingAddressInterface> {
        const address = await this.findById(id);

        if (null === address) {
            throw new CustomerShippingAddressNotFound(id);
        }

        return address;
    }

    async findById(id: CustomerShippingAddressIdInterface): Promise<CustomerShippingAddressInterface|null> {
        if (id.toString() in this.addresses) {
            return {...this.addresses[id.toString()]};
        }

        return null;
    }

    async findAllByCustomerId(customerId: CustomerIdInterface): Promise<CustomerShippingAddressInterface[]> {
        return Object.keys(this.addresses)
            .map(id => this.addresses[id])
            .filter(address => customerId.toString() === address.customerId.toString());
    }

    save(address: CustomerShippingAddressInterface): void {
        this.addresses[address.id.toString()] = address;
    }
}

interface AddressCollection {
    [index: string]: CustomerShippingAddressInterface
}