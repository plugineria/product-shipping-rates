import {CustomerShippingAddressIdInterface} from "../../../Domain/Model/Customer/ShippingAddress/CustomerShippingAddressIdInterface";

export class SetSessionShippingAddressCommand {
    constructor(
        readonly country: string|null = null,
        readonly postalCode: string|null = null,
        readonly customerShippingAddressId: CustomerShippingAddressIdInterface|null = null,
    ) {
    }
}