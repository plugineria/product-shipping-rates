export class SetSessionShippingAddressResult {
    static readonly ADDRESS_NOT_SPECIFIED = 'address_not_specified';
    static readonly CUSTOMER_ADDRESS_ID_NOT_FOUND = 'customer_address_id_not_found';
    static readonly POSTAL_CODE_DOES_NOT_EXIST_IN_COUNTRY = 'postal_code_does_not_exist_in_country';

    private constructor(
        readonly isValid: boolean = true,
        readonly error: string|null = null,
    ) {
    }

    static createValid(): SetSessionShippingAddressResult {
        return new SetSessionShippingAddressResult();
    }

    static createInvalidWithAddressNotSpecified(): SetSessionShippingAddressResult {
        return new SetSessionShippingAddressResult(
            false,
            SetSessionShippingAddressResult.ADDRESS_NOT_SPECIFIED
        );
    }

    static createInvalidWithCustomerAddressIdNotFound(): SetSessionShippingAddressResult {
        return new SetSessionShippingAddressResult(
            false,
            SetSessionShippingAddressResult.CUSTOMER_ADDRESS_ID_NOT_FOUND
        );
    }

    static createInvalidWithPostalCodeDoesNotExistInCountry(): SetSessionShippingAddressResult {
        return new SetSessionShippingAddressResult(
            false,
            SetSessionShippingAddressResult.POSTAL_CODE_DOES_NOT_EXIST_IN_COUNTRY
        );
    }
}