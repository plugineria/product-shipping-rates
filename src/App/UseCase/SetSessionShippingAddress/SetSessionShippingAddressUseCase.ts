import {SetSessionShippingAddressUseCasePort} from "./SetSessionShippingAddressUseCasePort";
import {SetSessionShippingAddressCommand} from "./SetSessionShippingAddressCommand";
import {SetSessionShippingAddressResult} from "./SetSessionShippingAddressResult";
import {SessionCustomerShippingAddressUpdater} from "../../../Domain/Service/ShippingAddress/SessionCustomerShippingAddressUpdater";
import {SessionPostalCodeAddressUpdater} from "../../../Domain/Service/ShippingAddress/SessionPostalCodeAddressUpdater";
import {CustomerShippingAddressNotFound} from "../../../Domain/Exception/CustomerShippingAddressNotFound";
import {PostalCodeAddressNotFound} from "../../../Domain/Exception/PostalCodeAddressNotFound";

export class SetSessionShippingAddressUseCase implements SetSessionShippingAddressUseCasePort {
    constructor(
        private customerShippingAddressUpdater: SessionCustomerShippingAddressUpdater,
        private postalCodeUpdater: SessionPostalCodeAddressUpdater,
    ) {
    }

    async execute(command: SetSessionShippingAddressCommand): Promise<SetSessionShippingAddressResult> {
        if (null !== command.customerShippingAddressId) {
            try {
                await this.customerShippingAddressUpdater.update(command.customerShippingAddressId);
            } catch (error: unknown) {
                if (error instanceof CustomerShippingAddressNotFound) {
                    return SetSessionShippingAddressResult.createInvalidWithCustomerAddressIdNotFound();
                } else {
                    throw error;
                }
            }
        } else if (null !== command.postalCode) {
            try {
                await this.postalCodeUpdater.update(command.postalCode, command.country);
            } catch (e: unknown) {
                if (e instanceof PostalCodeAddressNotFound) {
                    return SetSessionShippingAddressResult.createInvalidWithPostalCodeDoesNotExistInCountry();
                }
            }
        } else {
            return SetSessionShippingAddressResult.createInvalidWithAddressNotSpecified();
        }

        return SetSessionShippingAddressResult.createValid();
    }
}