import ProductMinimalShippingRateResolverInterface from "../../Domain/Service/ShippingRate/ProductMinimalShippingRateResolverInterface";
import {CacheInterface} from "./CacheInterface";
import {ProductIdInterface} from "../../Domain/Model/Product/ProductIdInterface";
import {AddressInterface} from "../../Domain/Model/Address/AddressInterface";
import {ShippingRateInterface} from "../../Domain/Model/ShippingRate/ShippingRateInterface";
import {Md5} from 'ts-md5/dist/md5';
import {ShippingRateFactoryInterface} from "../../Domain/Model/ShippingRate/ShippingRateFactoryInterface";

export class CachingProductMinimalShippingRateResolverDecorator implements ProductMinimalShippingRateResolverInterface {
    private static readonly CACHE_KEY_PREFIX = 'plugineria.product_shipping_price.min_price.';

    constructor(
        private realResolver: ProductMinimalShippingRateResolverInterface,
        private cache: CacheInterface,
        private ttl: number,
        private shippingRateFactory: ShippingRateFactoryInterface,
    ) {
    }

    async getMinimalShippingRate(
        productId: ProductIdInterface,
        shippingAddress: AddressInterface
    ): Promise<ShippingRateInterface|null> {
        const cacheKey = CachingProductMinimalShippingRateResolverDecorator.getCacheKey(productId, shippingAddress);

        const cachedShippingRate = await this.cache.get(cacheKey);
        const cachedMinimalShippingRate = cachedShippingRate
            ? this.shippingRateFactory.createFromString(cachedShippingRate)
            : null;

        if (cachedMinimalShippingRate) {
            return cachedMinimalShippingRate;
        }

        const minimalShippingRate = await this.realResolver.getMinimalShippingRate(productId, shippingAddress);

        if (null !== minimalShippingRate) {
            await this.cache.set(cacheKey, minimalShippingRate.toString(), this.ttl);
        }

        return minimalShippingRate;
    }

    private static getCacheKey(productId: ProductIdInterface, shippingAddress: AddressInterface): string {
        return CachingProductMinimalShippingRateResolverDecorator.CACHE_KEY_PREFIX
            + '.'
            + productId.toString()
            + '.'
            + Md5.hashStr(shippingAddress.toString());
    }
}