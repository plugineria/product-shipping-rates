import {ShippingMethodInterface} from "../../Domain/Model/ShippingMethod/ShippingMethodInterface";

export class ShippingRateView {
    constructor(
        readonly shippingMethod: ShippingMethodInterface,
        readonly price: number,
        readonly code: string,
        readonly title?: string | null,
        readonly description?: string | null,
    ) {
    }
}
