import {ShippingMethodRepositoryInterface} from "../../Domain/Repository/ShippingMethodRepositoryInterface";
import {ShippingRateInterface} from "../../Domain/Model/ShippingRate/ShippingRateInterface";
import {ShippingRateView} from "./ShippingRateView";

export class ShippingRateViewFactory {
    constructor(private shippingMethodRepository: ShippingMethodRepositoryInterface) {
    }

    async create(shippingRate: ShippingRateInterface): Promise<ShippingRateView> {
        return new ShippingRateView(
            await this.shippingMethodRepository.getById(shippingRate.shippingMethodId),
            shippingRate.price,
            shippingRate.code,
            shippingRate.title,
            shippingRate.description,
        );
    }
}