import {AddressInterface} from "../../Domain/Model/Address/AddressInterface";
import {CustomerShippingAddressInterface} from "../../Domain/Model/Customer/ShippingAddress/CustomerShippingAddressInterface";
import {CustomerShippingAddressIdInterface} from "../../Domain/Model/Customer/ShippingAddress/CustomerShippingAddressIdInterface";

export class SessionShippingAddressView {
    constructor(
        readonly address: AddressInterface,
        readonly customerShippingAddressId: CustomerShippingAddressIdInterface | null,
        readonly customerShippingAddresses: CustomerShippingAddressInterface[],
        readonly isProvidedByCustomer: boolean,
    ) {
    }
}