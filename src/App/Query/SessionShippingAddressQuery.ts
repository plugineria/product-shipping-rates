import {SessionShippingAddressQueryPort} from "./SessionShippingAddressQueryPort";
import {SessionShippingAddressView} from "../View/SessionShippingAddressView";
import {ShippingAddressResolverInterface} from "../../Domain/Service/ShippingAddress/ShippingAddressResolverInterface";
import CustomerShippingAddressRepositoryInterface from "../../Domain/Repository/CustomerShippingAddressRepositoryInterface";
import {CustomerSessionResolverInterface} from "../../Domain/Service/CustomerSessionResolverInterface";
import {ExampleShippingAddressResolverInterface} from "../../Domain/Service/ShippingAddress/ExampleShippingAddressResolverInterface";
import {CustomerShippingAddressInterface} from "../../Domain/Model/Customer/ShippingAddress/CustomerShippingAddressInterface";
import {AddressInterface} from "../../Domain/Model/Address/AddressInterface";
import {CustomerShippingAddressIdInterface} from "../../Domain/Model/Customer/ShippingAddress/CustomerShippingAddressIdInterface";

export class SessionShippingAddressQuery implements SessionShippingAddressQueryPort {
    constructor(
        private shippingAddressResolver: ShippingAddressResolverInterface,
        private customerShippingAddressRepository: CustomerShippingAddressRepositoryInterface,
        private customerSessionResolver: CustomerSessionResolverInterface,
        private exampleShippingAddressResolver: ExampleShippingAddressResolverInterface,
    ) {
    }

    async execute(): Promise<SessionShippingAddressView> {
        const address = await this.shippingAddressResolver.getDefaultShippingAddress();
        const customerId = await this.customerSessionResolver.getCustomerId();
        const customerShippingAddresses = customerId
            ? await this.customerShippingAddressRepository.findAllByCustomerId(customerId)
            : [];

        for (const customerShippingAddress of customerShippingAddresses) {
            if (customerShippingAddress.address.equals(address)) {
                return this.createSessionShippingAddressView(
                    address,
                    customerShippingAddress.id,
                    customerShippingAddresses
                );
            }
        }

        return await this.createSessionShippingAddressView(
            address,
            null,
            customerShippingAddresses
        );
    }

    private async createSessionShippingAddressView(
        address: AddressInterface,
        customerShippingAddressId: CustomerShippingAddressIdInterface | null,
        customerShippingAddresses: CustomerShippingAddressInterface[]
    ): Promise<SessionShippingAddressView> {
        return new SessionShippingAddressView(
            address,
            customerShippingAddressId,
            customerShippingAddresses,
            !(await this.exampleShippingAddressResolver.getExampleShippingAddress()).equals(address)
        );
    }
}