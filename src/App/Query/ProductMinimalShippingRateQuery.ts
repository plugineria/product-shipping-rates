import {ShippingRateView} from "../View/ShippingRateView";
import {ProductIdInterface} from "../../Domain/Model/Product/ProductIdInterface";
import ProductMinimalShippingRateQueryPort from "./ProductMinimalShippingRateQueryPort";
import ProductMinimalShippingRateResolverInterface from "../../Domain/Service/ShippingRate/ProductMinimalShippingRateResolverInterface";
import {ShippingAddressResolverInterface} from "../../Domain/Service/ShippingAddress/ShippingAddressResolverInterface";
import {ShippingRateViewFactory} from "../View/ShippingRateViewFactory";

export default class ProductMinimalShippingRateQuery implements ProductMinimalShippingRateQueryPort {
    constructor(
        private productMinimalShippingRateResolver: ProductMinimalShippingRateResolverInterface,
        private shippingAddressResolver: ShippingAddressResolverInterface,
        private shippingRateViewFactory: ShippingRateViewFactory,
    ) {
    }

    async execute(productId: ProductIdInterface): Promise<ShippingRateView|null> {
        const shippingRate = await this.productMinimalShippingRateResolver.getMinimalShippingRate(
            productId,
            await this.shippingAddressResolver.getDefaultShippingAddress()
        );

        return shippingRate ? await this.shippingRateViewFactory.create(shippingRate) : null;
    }
}
