import ProductShippingRatesQueryPort from "./ProductShippingRatesQueryPort";
import {ProductIdInterface} from "../../Domain/Model/Product/ProductIdInterface";
import {ShippingRateView} from "../View/ShippingRateView";
import {ProductShippingRatesResolver} from "../../Domain/Service/ShippingRate/ProductShippingRatesResolver";
import {ShippingAddressResolverInterface} from "../../Domain/Service/ShippingAddress/ShippingAddressResolverInterface";
import {ShippingRateViewFactory} from "../View/ShippingRateViewFactory";

export class ProductShippingRatesQuery implements ProductShippingRatesQueryPort {
    constructor(
        private productShippingRatesResolver: ProductShippingRatesResolver,
        private shippingAddressResolver: ShippingAddressResolverInterface,
        private shippingRateViewFactory: ShippingRateViewFactory,
    ) {
    }

    async execute(productId: ProductIdInterface): Promise<ShippingRateView[]> {
        const shippingAddress = await this.shippingAddressResolver.getDefaultShippingAddress();
        const shippingRates = await this.productShippingRatesResolver.getProductShippingRates(
            productId,
            shippingAddress,
        );

        return Promise.all(
            shippingRates.map(
                async shippingRate => await this.shippingRateViewFactory.create(shippingRate)
            )
        );
    }
}