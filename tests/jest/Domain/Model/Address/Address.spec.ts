import AddressTestBuilder from "../../../../../src/Infrastructure/Test/Builder/AddressTestBuilder";

const COUNTRY = 'US';
const REGION = 'TX';
const CITY = 'Austin';
const POSTAL_CODE = '00111';
const STREET = 'Main st 2020';

it('serializes address to string', () => {
    // Arrange
    const address = AddressTestBuilder.create()
        .withCountry(COUNTRY)
        .withRegion(REGION)
        .withCity(CITY)
        .withPostalCode(POSTAL_CODE)
        .withStreet(STREET)
        .build();

    // Act + Assert
    expect(address.toString()).toEqual(
        JSON.stringify({
            "country": COUNTRY,
            "region": REGION,
            "postalCode": POSTAL_CODE,
            "city": CITY,
            "street": STREET,
        })
    )
});