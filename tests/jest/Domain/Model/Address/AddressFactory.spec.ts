import AddressTestBuilder from "../../../../../src/Infrastructure/Test/Builder/AddressTestBuilder";
import {AddressFactory} from "../../../../../src/Domain/Model/Address/AddressFactory";
import {Address} from "../../../../../src/Domain/Model/Address/Address";
import {AddressAttributeNotFound} from "../../../../../src/Domain/Exception/AddressAttributeNotFound";

const COUNTRY = 'US';
const REGION = 'TX';
const CITY = 'Austin';
const POSTAL_CODE = '00111';
const STREET = 'Main st 2020';

it('Create From Json With All Values Gets New Object', () => {
    // Arrange
    const json = JSON.stringify({
        "country": COUNTRY,
        "region": REGION,
        "postalCode": POSTAL_CODE,
        "city": CITY,
        "street": STREET,
    });

    // Act
    const address = new AddressFactory().createFromString(json);

    // Assert
    expect(address).toEqual(
        new Address(COUNTRY, CITY, POSTAL_CODE, STREET, REGION)
    )
});

it('Create From Json With Minimal Values Gets New Object', () => {
    // Arrange
    const json = JSON.stringify({
        "country": COUNTRY,
        "city": CITY,
        "hello": "bye",
    });

    // Act
    const address = new AddressFactory().createFromString(json);

    // Assert
    expect(address).toEqual(
        new Address(COUNTRY, CITY)
    )
});

it('Create From Json Without Country Throws Address Attribute Not Found Error', () => {
    // Arrange
    const json = JSON.stringify({
        "city": CITY,
        "hello": "bye",
    });

    // Act + Assert
    expect(() => new AddressFactory().createFromString(json)).toThrow(
        AddressAttributeNotFound
    )
});

it('Create From Json Without City Throws Address Attribute Not Found Error', () => {
    // Arrange
    const json = JSON.stringify({
        "country": COUNTRY,
        "hello": "bye",
    });

    // Act + Assert
    expect(() => new AddressFactory().createFromString(json)).toThrow(
        AddressAttributeNotFound
    )
});