import {ShippingAddressResolver} from "../../../../../src/Domain/Service/ShippingAddress/ShippingAddressResolver";
import {InMemorySessionShippingAddressRepository} from "../../../../../src/Infrastructure/InMemory/Repository/InMemorySessionShippingAddressRepository";
import {MockedCustomerShippingAddressResolver} from "../../../../../src/Infrastructure/Mock/Service/ShippingAddress/MockedCustomerShippingAddressResolver";
import {MockedCustomerSessionResolver} from "../../../../../src/Infrastructure/Mock/Service/MockedCustomerSessionResolver";
import AddressTestBuilder from "../../../../../src/Infrastructure/Test/Builder/AddressTestBuilder";
import {Address} from "../../../../../src/Domain/Model/Address/Address";
import {MockedExampleShippingAddressResolver} from "../../../../../src/Infrastructure/Mock/Service/ShippingAddress/MockedExampleShippingAddressResolver";
import {CustomerId} from "../../../../../src/Domain/Model/Customer/CustomerId";
import CustomerShippingAddressTestBuilder
    from "../../../../../src/Infrastructure/Test/Builder/CustomerShippingAddressTestBuilder";

const arrangeShippingAddressResolver = (): [
    ShippingAddressResolver,
    InMemorySessionShippingAddressRepository,
    MockedCustomerShippingAddressResolver,
    MockedCustomerSessionResolver,
    Address,
] => {
    const sessionShippingAddressRepository = new InMemorySessionShippingAddressRepository();
    const customerShippingAddressResolverMock = new MockedCustomerShippingAddressResolver();
    const customerSessionResolverMock = new MockedCustomerSessionResolver();
    const exampleShippingAddress = AddressTestBuilder.create().build();
    const exampleShippingAddressResolver = new MockedExampleShippingAddressResolver();
    exampleShippingAddressResolver.exampleShippingAddress = exampleShippingAddress;

    return [
        new ShippingAddressResolver(
            sessionShippingAddressRepository,
            customerShippingAddressResolverMock,
            customerSessionResolverMock,
            exampleShippingAddressResolver,
        ),
        sessionShippingAddressRepository,
        customerShippingAddressResolverMock,
        customerSessionResolverMock,
        exampleShippingAddress,
    ];
}

it('returns example address for empty customer session', async () => {
    // Arrange
    const [
        shippingAddressResolver,
        ,
        ,
        customerSessionResolverMock,
        exampleShippingAddress,
    ] = arrangeShippingAddressResolver();
    customerSessionResolverMock.customerId = null;

    // Act
    const defaultShippingAddress = await shippingAddressResolver.getDefaultShippingAddress();

    // Assert
    expect(defaultShippingAddress).toEqual(exampleShippingAddress);
});

it('returns example address for customer without address', () => {
    // Arrange
    const customerId = new CustomerId('customer1');
    const [
        shippingAddressResolver,
        sessionShippingAddressRepository,
        ,
        customerSessionResolverMock,
        exampleShippingAddress,
    ] = arrangeShippingAddressResolver();
    customerSessionResolverMock.customerId = customerId;

    // Act
    const defaultShippingAddress = shippingAddressResolver.getDefaultShippingAddress();
    const sessionShippingAddress = sessionShippingAddressRepository.get();

    // Assert
    expect(defaultShippingAddress).resolves.toEqual(exampleShippingAddress);
    expect(sessionShippingAddress).resolves.toBeNull();
});

it('returns default customer address and saves in session repository', async () => {
    // Arrange
    const customerId = new CustomerId('customer1');
    const customerShippingAddress = new CustomerShippingAddressTestBuilder()
        .withCustomerId(customerId)
        .build();

    const [
        shippingAddressResolver,
        sessionShippingAddressRepository,
        customerShippingAddressResolverMock,
        customerSessionResolverMock,
    ] = arrangeShippingAddressResolver();

    customerSessionResolverMock.customerId = customerId;
    customerShippingAddressResolverMock.setCustomerPrimaryShippingAddress(customerShippingAddress);


    // Act
    const defaultShippingAddress = await shippingAddressResolver.getDefaultShippingAddress();
    const sessionShippingAddress = await sessionShippingAddressRepository.get();

    // Assert
    expect(defaultShippingAddress).toEqual(customerShippingAddress.address);
    expect(sessionShippingAddress).toEqual(customerShippingAddress.address);
});

it('returns shipping address recorded in session address repository', () => {
    // Arrange
    const address = AddressTestBuilder.create().build();
    const [
        shippingAddressResolver,
        sessionShippingAddressRepository,
    ] = arrangeShippingAddressResolver();
    sessionShippingAddressRepository.save(address);

    // Act
    const defaultShippingAddress = shippingAddressResolver.getDefaultShippingAddress();

    // Assert
    expect(defaultShippingAddress).resolves.toEqual(address);
});
