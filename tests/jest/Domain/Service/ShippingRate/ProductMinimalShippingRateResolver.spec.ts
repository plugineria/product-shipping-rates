import {mock, MockProxy} from "jest-mock-extended";
import {ProductMinimalShippingRateResolver} from "../../../../../src/Domain/Service/ShippingRate/ProductMinimalShippingRateResolver";
import {ProductId} from "../../../../../src/Domain/Model/Product/ProductId";
import AddressTestBuilder from "../../../../../src/Infrastructure/Test/Builder/AddressTestBuilder";
import {ShippingMethodId} from "../../../../../src/Domain/Model/ShippingMethod/ShippingMethodId";
import ShippingRateTestBuilder from "../../../../../src/Infrastructure/Test/Builder/ShippingRateTestBuilder";
import {ShippingMethod} from "../../../../../src/Domain/Model/ShippingMethod/ShippingMethod";
import {ShippingRate} from "../../../../../src/Domain/Model/ShippingRate/ShippingRate";
import {ShippingRateInterface} from "../../../../../src/Domain/Model/ShippingRate/ShippingRateInterface";
import {ProductShippingRatesResolver} from "../../../../../src/Domain/Service/ShippingRate/ProductShippingRatesResolver";

const arrangeProductMinimalShippingRateResolver = (): [
    ProductMinimalShippingRateResolver,
    MockProxy<ProductShippingRatesResolver>,
] => {
    const productShippingRatesResolverMock = mock<ProductShippingRatesResolver>();

    return [
        new ProductMinimalShippingRateResolver(
            productShippingRatesResolverMock
        ),
        productShippingRatesResolverMock,
    ];
}

it("is empty with no shipping rates", () => {
    // Arrange
    const [
        productMinimalShippingRateResolver,
        productShippingRatesResolverMock,
    ] = arrangeProductMinimalShippingRateResolver();
    const productId = new ProductId('product1');
    const shippingAddress = AddressTestBuilder.create().build();
    productShippingRatesResolverMock.getProductShippingRates.mockReturnValue(
        new Promise<ShippingRateInterface[]>(resolve => [])
    );

    // Act
    const shippingRatePromise = productMinimalShippingRateResolver.getMinimalShippingRate(productId, shippingAddress);

    // Assert
    expect(shippingRatePromise).resolves.toBeNull();
});

it("is minimal from 3 shipping methods and one does not have rate", () => {
    interface ShippingMethodToRate {
        method: ShippingMethod,
        rate: ShippingRate|null,
    }

    // Arrange
    const productId = new ProductId('product1');
    const shippingAddress = AddressTestBuilder.create().build();
    const [
        productMinimalShippingRateResolver,
        productShippingRatesResolverMock,
    ] = arrangeProductMinimalShippingRateResolver();
    const shippingRates: ShippingRate[] = [
        ShippingRateTestBuilder.create()
            .withShippingMethodId(new ShippingMethodId('sm1'))
            .withPrice(10)
            .build(),
        ShippingRateTestBuilder.create()
            .withShippingMethodId(new ShippingMethodId('sm2'))
            .withPrice(5)
            .build(),
        ShippingRateTestBuilder.create()
            .withShippingMethodId(new ShippingMethodId('sm3'))
            .withPrice(50)
            .build(),
    ];
    productShippingRatesResolverMock
        .getProductShippingRates
        .calledWith(productId, shippingAddress)
        .mockReturnValue(
            new Promise<ShippingRateInterface[]>(resolve => shippingRates)
        );

    // Act
    const minimalRatePromise = productMinimalShippingRateResolver.getMinimalShippingRate(productId, shippingAddress);

    // Assert
    expect(minimalRatePromise).resolves.toEqual(shippingRates[1]);
});
