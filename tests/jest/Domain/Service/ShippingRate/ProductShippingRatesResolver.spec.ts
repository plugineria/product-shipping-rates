import {ProductId} from "../../../../../src/Domain/Model/Product/ProductId";
import ShippingMethodTestBuilder from "../../../../../src/Infrastructure/Test/Builder/ShippingMethodTestBuilder";
import {ShippingMethodId} from "../../../../../src/Domain/Model/ShippingMethod/ShippingMethodId";
import ShippingRateTestBuilder from "../../../../../src/Infrastructure/Test/Builder/ShippingRateTestBuilder";
import AddressTestBuilder from "../../../../../src/Infrastructure/Test/Builder/AddressTestBuilder";
import {ProductShippingRatesResolver} from "../../../../../src/Domain/Service/ShippingRate/ProductShippingRatesResolver";
import {MockedAvailableShippingMethodsResolver} from "../../../../../src/Infrastructure/Mock/Service/MockedAvailableShippingMethodsResolver";
import {MockedShippingRateResolver} from "../../../../../src/Infrastructure/Mock/Service/ShippingRate/MockedShippingRateResolver";
import {InMemoryShippingMethodRepository} from "../../../../../src/Infrastructure/InMemory/Repository/InMemoryShippingMethodRepository";
import {ShippingRate} from "../../../../../src/Domain/Model/ShippingRate/ShippingRate";

const arrangeProductShippingRatesResolver = (): [
    ProductShippingRatesResolver,
    MockedAvailableShippingMethodsResolver,
    MockedShippingRateResolver,
    InMemoryShippingMethodRepository,
] => {
    const shippingMethodRepository = new InMemoryShippingMethodRepository();
    const availableShippingMethodsResolver = new MockedAvailableShippingMethodsResolver(shippingMethodRepository);
    const shippingRateResolver = new MockedShippingRateResolver();

    return [
        new ProductShippingRatesResolver(
            availableShippingMethodsResolver,
            shippingRateResolver,
        ),
        availableShippingMethodsResolver,
        shippingRateResolver,
        shippingMethodRepository,
    ];
}

it('should combine all rates from all available shipping methods', () => {
    // Arrange
    const productId = new ProductId('product1');
    const shippingAddress = AddressTestBuilder.create().build();
    const shippingMethod1 = ShippingMethodTestBuilder.create()
        .withId(new ShippingMethodId('shippingMethod1'))
        .build();
    const shippingMethod2 = ShippingMethodTestBuilder.create()
        .withId(new ShippingMethodId('shippingMethod2'))
        .build();
    const shippingMethod3 = ShippingMethodTestBuilder.create()
        .withId(new ShippingMethodId('shippingMethod3'))
        .build();
    const method1rates: ShippingRate[] = [
        ShippingRateTestBuilder.create()
            .withShippingMethodId(shippingMethod1.id)
            .withTitle('method1_rate1')
            .build(),
        ShippingRateTestBuilder.create()
            .withShippingMethodId(shippingMethod1.id)
            .withTitle('method1_rate2')
            .build(),
    ];
    const method3rates = [
        ShippingRateTestBuilder.create()
            .withShippingMethodId(shippingMethod3.id)
            .withTitle('method3_rate1')
            .build(),
        ShippingRateTestBuilder.create()
            .withShippingMethodId(shippingMethod3.id)
            .withTitle('method3_rate2')
            .build(),
    ];

    const [
        productShippingRatesResolver,
        availableShippingMethodsResolver,
        shippingRateResolver,
        shippingMethodRepository,
    ] = arrangeProductShippingRatesResolver();
    shippingMethodRepository.save(
        shippingMethod1,
        shippingMethod2,
        shippingMethod3,
    );
    availableShippingMethodsResolver.shippingMethodIds = [
        shippingMethod1.id,
        shippingMethod2.id,
        shippingMethod3.id,
    ];
    shippingRateResolver.ratesPerShippingMethod[shippingMethod1.id.toString()] = method1rates;
    shippingRateResolver.ratesPerShippingMethod[shippingMethod2.id.toString()] = [];
    shippingRateResolver.ratesPerShippingMethod[shippingMethod3.id.toString()] = method3rates;

    // Act
    const productShippingRates = productShippingRatesResolver.getProductShippingRates(productId, shippingAddress);

    // Assert
    expect(productShippingRates).resolves.toEqual(method1rates.concat(method3rates));
});