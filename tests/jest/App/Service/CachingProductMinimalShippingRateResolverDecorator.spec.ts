import {CachingProductMinimalShippingRateResolverDecorator} from "../../../../src/App/Service/CachingProductMinimalShippingRateResolverDecorator";
import {ProductMinimalShippingRateResolver} from "../../../../src/Domain/Service/ShippingRate/ProductMinimalShippingRateResolver";
import {mock, MockProxy} from "jest-mock-extended";
import {InMemoryCache} from "../../../../src/Infrastructure/InMemory/Service/InMemoryCache";
import {ShippingRateFactory} from "../../../../src/Domain/Model/ShippingRate/ShippingRateFactory";
import {ProductId} from "../../../../src/Domain/Model/Product/ProductId";
import AddressTestBuilder from "../../../../src/Infrastructure/Test/Builder/AddressTestBuilder";
import ShippingRateTestBuilder from "../../../../src/Infrastructure/Test/Builder/ShippingRateTestBuilder";

const arrangeDecorator = (): [
    CachingProductMinimalShippingRateResolverDecorator,
    MockProxy<ProductMinimalShippingRateResolver>,
    InMemoryCache,
    number,
    ShippingRateFactory,
] => {
    const resolverMock = mock<ProductMinimalShippingRateResolver>();
    const cache = new InMemoryCache();
    const ttl = 1;
    const shippingRateFactory = new ShippingRateFactory();

    return [
        new CachingProductMinimalShippingRateResolverDecorator(
            resolverMock,
            cache,
            ttl,
            shippingRateFactory,
        ),
        resolverMock,
        cache,
        ttl,
        shippingRateFactory,
    ];
}

it('saves cache with shipping address provided', async () => {
    // Arrange
    const productId = new ProductId('product1');
    const shippingAddress = AddressTestBuilder.create().build();
    const shippingRate = ShippingRateTestBuilder.create().build();

    const [decorator, resolverMock, cache] = arrangeDecorator();

    resolverMock.getMinimalShippingRate
        .calledWith(productId, shippingAddress)
        .mockResolvedValue(shippingRate);

    // Act
    const minimalShippingRate = await decorator.getMinimalShippingRate(productId, shippingAddress);

    // Assert
    expect(minimalShippingRate).toEqual(shippingRate);
    expect(
        Object.values(cache.getAll()).map(item => item.value)
    ).toEqual(
        [shippingRate.toString()]
    );
});

it('uses cached value for second call', async () => {
    // Arrange
    const productId = new ProductId('product1');
    const shippingAddress = AddressTestBuilder.create().build();
    const shippingRate = ShippingRateTestBuilder.create().build();

    const [decorator, resolverMock, cache] = arrangeDecorator();

    resolverMock.getMinimalShippingRate
        .calledWith(productId, shippingAddress)
        .mockResolvedValue(shippingRate);

    await decorator.getMinimalShippingRate(productId, shippingAddress);

    // Act
    const secondResult = await decorator.getMinimalShippingRate(productId, shippingAddress);

    // Assert
    expect(secondResult).toEqual(shippingRate);
    expect(resolverMock.getMinimalShippingRate).toBeCalledTimes(1);
});

it('calls decorated resolver after cache expired on second call', async () => {
    // Arrange
    const productId = new ProductId('product1');
    const shippingAddress = AddressTestBuilder.create().build();
    const shippingRate = ShippingRateTestBuilder.create().build();

    const [decorator, resolverMock, cache, ttl] = arrangeDecorator();

    resolverMock.getMinimalShippingRate
        .calledWith(productId, shippingAddress)
        .mockResolvedValue(shippingRate);

    await decorator.getMinimalShippingRate(productId, shippingAddress);
    const delay = async (ms: number) => new Promise( resolve => setTimeout(resolve, ms));
    await delay(ttl * 100 + 100);

    // Act
    const secondResult = await decorator.getMinimalShippingRate(productId, shippingAddress);

    // Assert
    expect(secondResult).toEqual(shippingRate);
    expect(resolverMock.getMinimalShippingRate).toBeCalledTimes(2);
});

