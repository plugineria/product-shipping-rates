import { CacheInterface } from "../../../App/Service/CacheInterface";
export declare class InMemoryCache implements CacheInterface {
    private storage;
    get(key: string): Promise<string | null>;
    set(key: string, value: string, ttlSec?: number | null): Promise<void>;
    getAll(): InMemoryCacheStorage;
}
interface InMemoryCacheStorage {
    [index: string]: InMemoryCacheEntity;
}
interface InMemoryCacheEntity {
    readonly value: string;
    readonly ttl: number | null;
    readonly timestamp: number;
}
export {};
