import CustomerShippingAddressRepositoryInterface from "../../../Domain/Repository/CustomerShippingAddressRepositoryInterface";
import { CustomerShippingAddressIdInterface } from "../../../Domain/Model/Customer/ShippingAddress/CustomerShippingAddressIdInterface";
import { CustomerShippingAddressInterface } from "../../../Domain/Model/Customer/ShippingAddress/CustomerShippingAddressInterface";
import { CustomerIdInterface } from "../../../Domain/Model/Customer/CustomerIdInterface";
export declare class InMemoryCustomerShippingAddressRepository implements CustomerShippingAddressRepositoryInterface {
    private addresses;
    constructor(addresses?: AddressCollection);
    getById(id: CustomerShippingAddressIdInterface): Promise<CustomerShippingAddressInterface>;
    findById(id: CustomerShippingAddressIdInterface): Promise<CustomerShippingAddressInterface | null>;
    findAllByCustomerId(customerId: CustomerIdInterface): Promise<CustomerShippingAddressInterface[]>;
    save(address: CustomerShippingAddressInterface): void;
}
interface AddressCollection {
    [index: string]: CustomerShippingAddressInterface;
}
export {};
