import { ShippingMethodRepositoryInterface } from "../../../Domain/Repository/ShippingMethodRepositoryInterface";
import { ShippingMethodInterface } from "../../../Domain/Model/ShippingMethod/ShippingMethodInterface";
import { ShippingMethodIdInterface } from "../../../Domain/Model/ShippingMethod/ShippingMethodIdInterface";
export declare class InMemoryShippingMethodRepository implements ShippingMethodRepositoryInterface {
    private shippingMethods;
    findById(id: ShippingMethodIdInterface): Promise<ShippingMethodInterface | null>;
    getById(id: ShippingMethodIdInterface): Promise<ShippingMethodInterface>;
    save(...shippingMethods: ShippingMethodInterface[]): void;
}
