import SessionShippingAddressRepositoryInterface from "../../../Domain/Repository/SessionShippingAddressRepositoryInterface";
import { AddressInterface } from "../../../Domain/Model/Address/AddressInterface";
export declare class InMemorySessionShippingAddressRepository implements SessionShippingAddressRepositoryInterface {
    private address;
    constructor(address?: AddressInterface | null);
    get(): Promise<AddressInterface | null>;
    save(address: AddressInterface): Promise<void>;
    reset(): void;
}
