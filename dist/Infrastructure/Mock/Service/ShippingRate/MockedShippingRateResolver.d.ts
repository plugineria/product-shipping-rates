import { ShippingRateResolverInterface } from "../../../../Domain/Service/ShippingRate/ShippingRateResolverInterface";
import { ShippingMethodIdInterface } from "../../../../Domain/Model/ShippingMethod/ShippingMethodIdInterface";
import { ProductIdInterface } from "../../../../Domain/Model/Product/ProductIdInterface";
import { AddressInterface } from "../../../../Domain/Model/Address/AddressInterface";
import { ShippingRateInterface } from "../../../../Domain/Model/ShippingRate/ShippingRateInterface";
export declare class MockedShippingRateResolver implements ShippingRateResolverInterface {
    ratesPerShippingMethod: RatesCollectionByShippingMethodId;
    getRates(shippingMethodId: ShippingMethodIdInterface, productId: ProductIdInterface, shippingAddress: AddressInterface): Promise<ShippingRateInterface[]>;
}
interface RatesCollectionByShippingMethodId {
    [key: string]: ShippingRateInterface[];
}
export {};
