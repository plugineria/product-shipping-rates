import { PostalCodeAddressResolverInterface } from "../../../Domain/Service/PostalCodeAddressResolverInterface";
import { Address } from "../../../Domain/Model/Address/Address";
export declare class MockedPostalCodeAddressResolver implements PostalCodeAddressResolverInterface {
    city: string | null;
    getAddress(postalCode: string, country: string): Promise<Address | null>;
}
