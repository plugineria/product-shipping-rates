import AvailableShippingMethodsResolverInterface from "../../../Domain/Service/AvailableShippingMethodsResolverInterface";
import { ShippingMethodRepositoryInterface } from "../../../Domain/Repository/ShippingMethodRepositoryInterface";
import { ProductIdInterface } from "../../../Domain/Model/Product/ProductIdInterface";
import { ShippingMethodInterface } from "../../../Domain/Model/ShippingMethod/ShippingMethodInterface";
import { ShippingMethodIdInterface } from "../../../Domain/Model/ShippingMethod/ShippingMethodIdInterface";
export declare class MockedAvailableShippingMethodsResolver implements AvailableShippingMethodsResolverInterface {
    private shippingMethodRepository;
    shippingMethodIds: ShippingMethodIdInterface[];
    constructor(shippingMethodRepository: ShippingMethodRepositoryInterface);
    getAvailableShippingMethods(productId: ProductIdInterface): Promise<ShippingMethodInterface[]>;
}
