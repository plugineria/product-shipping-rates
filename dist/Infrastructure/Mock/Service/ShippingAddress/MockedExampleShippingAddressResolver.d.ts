import { ExampleShippingAddressResolverInterface } from "../../../../Domain/Service/ShippingAddress/ExampleShippingAddressResolverInterface";
import { AddressInterface } from "../../../../Domain/Model/Address/AddressInterface";
export declare class MockedExampleShippingAddressResolver implements ExampleShippingAddressResolverInterface {
    exampleShippingAddress: AddressInterface | null;
    getExampleShippingAddress(): Promise<AddressInterface>;
}
