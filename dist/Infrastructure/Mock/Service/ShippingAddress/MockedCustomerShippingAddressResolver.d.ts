import { CustomerShippingAddressResolverInterface } from "../../../../Domain/Service/ShippingAddress/CustomerShippingAddressResolverInterface";
import { CustomerIdInterface } from "../../../../Domain/Model/Customer/CustomerIdInterface";
import { CustomerShippingAddressInterface } from "../../../../Domain/Model/Customer/ShippingAddress/CustomerShippingAddressInterface";
export declare class MockedCustomerShippingAddressResolver implements CustomerShippingAddressResolverInterface {
    private defaultCustomerShippingAddresses;
    constructor(defaultCustomerShippingAddresses?: PrimaryCustomerShippingAddressCollectionByCustomerId);
    getPrimaryCustomerShippingAddress(customerId: CustomerIdInterface): Promise<CustomerShippingAddressInterface | null>;
    setCustomerPrimaryShippingAddress(address: CustomerShippingAddressInterface): void;
}
interface PrimaryCustomerShippingAddressCollectionByCustomerId {
    [key: string]: CustomerShippingAddressInterface | null;
}
export {};
