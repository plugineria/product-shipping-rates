import { CustomerSessionResolverInterface } from "../../../Domain/Service/CustomerSessionResolverInterface";
import { CustomerIdInterface } from "../../../Domain/Model/Customer/CustomerIdInterface";
export declare class MockedCustomerSessionResolver implements CustomerSessionResolverInterface {
    customerId: CustomerIdInterface | null;
    getCustomerId(): Promise<CustomerIdInterface | null>;
}
