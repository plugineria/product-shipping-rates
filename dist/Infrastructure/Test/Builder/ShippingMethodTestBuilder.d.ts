import { ShippingMethod } from "../../../Domain/Model/ShippingMethod/ShippingMethod";
import { ShippingMethodIdInterface } from "../../../Domain/Model/ShippingMethod/ShippingMethodIdInterface";
export default class ShippingMethodTestBuilder {
    static readonly DEFAULT_ID: string;
    static readonly DEFAULT_NAME: string;
    private id;
    private title;
    private constructor();
    static create(): ShippingMethodTestBuilder;
    build(): ShippingMethod;
    withId(id: ShippingMethodIdInterface): ShippingMethodTestBuilder;
    withTitle(title: string): ShippingMethodTestBuilder;
}
