import { CustomerShippingAddressIdInterface } from "../../../Domain/Model/Customer/ShippingAddress/CustomerShippingAddressIdInterface";
import { CustomerIdInterface } from "../../../Domain/Model/Customer/CustomerIdInterface";
import { AddressInterface } from "../../../Domain/Model/Address/AddressInterface";
import { CustomerShippingAddress } from "../../../Domain/Model/Customer/ShippingAddress/CustomerShippingAddress";
export default class CustomerShippingAddressTestBuilder {
    private id;
    private customerId;
    private address;
    build(): CustomerShippingAddress;
    withId(id: CustomerShippingAddressIdInterface): CustomerShippingAddressTestBuilder;
    withCustomerId(customerId: CustomerIdInterface): CustomerShippingAddressTestBuilder;
    withAddress(address: AddressInterface): CustomerShippingAddressTestBuilder;
}
