import { Address } from "../../../Domain/Model/Address/Address";
export default class AddressTestBuilder {
    private country;
    private city;
    private postalCode;
    private region;
    private street;
    static readonly COUNTRY = "PL";
    static readonly CITY = "Warszawa";
    private constructor();
    static create(): AddressTestBuilder;
    build(): Address;
    withCountry(value: string): AddressTestBuilder;
    withCity(value: string): AddressTestBuilder;
    withPostalCode(value: string | null): AddressTestBuilder;
    withRegion(value: string | null): AddressTestBuilder;
    withStreet(value: string | null): AddressTestBuilder;
}
