"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ShippingMethod_1 = require("../../../Domain/Model/ShippingMethod/ShippingMethod");
var ShippingMethodId_1 = require("../../../Domain/Model/ShippingMethod/ShippingMethodId");
var ShippingMethodTestBuilder = /** @class */ (function () {
    function ShippingMethodTestBuilder() {
        this.id = new ShippingMethodId_1.ShippingMethodId(ShippingMethodTestBuilder.DEFAULT_ID);
        this.title = ShippingMethodTestBuilder.DEFAULT_NAME;
    }
    ShippingMethodTestBuilder.create = function () {
        return new ShippingMethodTestBuilder();
    };
    ShippingMethodTestBuilder.prototype.build = function () {
        return new ShippingMethod_1.ShippingMethod(this.id, this.title);
    };
    ShippingMethodTestBuilder.prototype.withId = function (id) {
        this.id = id;
        return this;
    };
    ShippingMethodTestBuilder.prototype.withTitle = function (title) {
        this.title = title;
        return this;
    };
    ShippingMethodTestBuilder.DEFAULT_ID = 'dpd';
    ShippingMethodTestBuilder.DEFAULT_NAME = 'DPD';
    return ShippingMethodTestBuilder;
}());
exports.default = ShippingMethodTestBuilder;
