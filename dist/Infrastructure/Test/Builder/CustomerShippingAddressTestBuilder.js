"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var AddressTestBuilder_1 = __importDefault(require("./AddressTestBuilder"));
var CustomerId_1 = require("../../../Domain/Model/Customer/CustomerId");
var crypto = __importStar(require("crypto"));
var CustomerShippingAddressId_1 = require("../../../Domain/Model/Customer/ShippingAddress/CustomerShippingAddressId");
var CustomerShippingAddress_1 = require("../../../Domain/Model/Customer/ShippingAddress/CustomerShippingAddress");
var CustomerShippingAddressTestBuilder = /** @class */ (function () {
    function CustomerShippingAddressTestBuilder() {
        this.id = new CustomerShippingAddressId_1.CustomerShippingAddressId("customer_shipping_address_" + crypto.randomBytes(20).toString('hex'));
        this.customerId = new CustomerId_1.CustomerId("customer_" + crypto.randomBytes(20).toString('hex'));
        this.address = AddressTestBuilder_1.default.create().build();
    }
    CustomerShippingAddressTestBuilder.prototype.build = function () {
        return new CustomerShippingAddress_1.CustomerShippingAddress(this.id, this.customerId, this.address);
    };
    CustomerShippingAddressTestBuilder.prototype.withId = function (id) {
        this.id = id;
        return this;
    };
    CustomerShippingAddressTestBuilder.prototype.withCustomerId = function (customerId) {
        this.customerId = customerId;
        return this;
    };
    CustomerShippingAddressTestBuilder.prototype.withAddress = function (address) {
        this.address = address;
        return this;
    };
    return CustomerShippingAddressTestBuilder;
}());
exports.default = CustomerShippingAddressTestBuilder;
