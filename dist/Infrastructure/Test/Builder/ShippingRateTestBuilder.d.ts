import { ShippingMethodIdInterface } from "../../../Domain/Model/ShippingMethod/ShippingMethodIdInterface";
import { ShippingRate } from "../../../Domain/Model/ShippingRate/ShippingRate";
export default class ShippingRateTestBuilder {
    static readonly DEFAULT_PRICE = 15.99;
    static readonly DEFAULT_CODE = "standard";
    private shippingMethodId;
    private code;
    private price;
    private title;
    private description;
    private constructor();
    static create(): ShippingRateTestBuilder;
    build(): ShippingRate;
    withShippingMethodId(shippingMethodId: ShippingMethodIdInterface): ShippingRateTestBuilder;
    withCode(code: string): ShippingRateTestBuilder;
    withPrice(price: number): ShippingRateTestBuilder;
    withTitle(title: string | null): ShippingRateTestBuilder;
    withDescription(description: string | null): ShippingRateTestBuilder;
}
