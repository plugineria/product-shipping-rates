"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Address_1 = require("../../../Domain/Model/Address/Address");
var AddressTestBuilder = /** @class */ (function () {
    function AddressTestBuilder(country, city, postalCode, region, street) {
        if (country === void 0) { country = AddressTestBuilder.COUNTRY; }
        if (city === void 0) { city = AddressTestBuilder.CITY; }
        if (postalCode === void 0) { postalCode = null; }
        if (region === void 0) { region = null; }
        if (street === void 0) { street = null; }
        this.country = country;
        this.city = city;
        this.postalCode = postalCode;
        this.region = region;
        this.street = street;
    }
    AddressTestBuilder.create = function () {
        return new AddressTestBuilder();
    };
    AddressTestBuilder.prototype.build = function () {
        return new Address_1.Address(this.country, this.city, this.postalCode, this.street, this.region);
    };
    AddressTestBuilder.prototype.withCountry = function (value) {
        this.country = value;
        return this;
    };
    AddressTestBuilder.prototype.withCity = function (value) {
        this.city = value;
        return this;
    };
    AddressTestBuilder.prototype.withPostalCode = function (value) {
        this.postalCode = value;
        return this;
    };
    AddressTestBuilder.prototype.withRegion = function (value) {
        this.region = value;
        return this;
    };
    AddressTestBuilder.prototype.withStreet = function (value) {
        this.street = value;
        return this;
    };
    AddressTestBuilder.COUNTRY = 'PL';
    AddressTestBuilder.CITY = 'Warszawa';
    return AddressTestBuilder;
}());
exports.default = AddressTestBuilder;
