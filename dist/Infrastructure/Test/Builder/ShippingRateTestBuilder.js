"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var ShippingMethodId_1 = require("../../../Domain/Model/ShippingMethod/ShippingMethodId");
var ShippingMethodTestBuilder_1 = __importDefault(require("./ShippingMethodTestBuilder"));
var ShippingRate_1 = require("../../../Domain/Model/ShippingRate/ShippingRate");
var ShippingRateTestBuilder = /** @class */ (function () {
    function ShippingRateTestBuilder() {
        this.shippingMethodId = new ShippingMethodId_1.ShippingMethodId(ShippingMethodTestBuilder_1.default.DEFAULT_ID);
        this.code = ShippingRateTestBuilder.DEFAULT_CODE;
        this.price = ShippingRateTestBuilder.DEFAULT_PRICE;
        this.title = null;
        this.description = null;
    }
    ShippingRateTestBuilder.create = function () {
        return new ShippingRateTestBuilder();
    };
    ShippingRateTestBuilder.prototype.build = function () {
        return new ShippingRate_1.ShippingRate(this.shippingMethodId, this.code, this.price, this.title, this.description);
    };
    ShippingRateTestBuilder.prototype.withShippingMethodId = function (shippingMethodId) {
        this.shippingMethodId = shippingMethodId;
        return this;
    };
    ShippingRateTestBuilder.prototype.withCode = function (code) {
        this.code = code;
        return this;
    };
    ShippingRateTestBuilder.prototype.withPrice = function (price) {
        this.price = price;
        return this;
    };
    ShippingRateTestBuilder.prototype.withTitle = function (title) {
        this.title = title;
        return this;
    };
    ShippingRateTestBuilder.prototype.withDescription = function (description) {
        this.description = description;
        return this;
    };
    ShippingRateTestBuilder.DEFAULT_PRICE = 15.99;
    ShippingRateTestBuilder.DEFAULT_CODE = 'standard';
    return ShippingRateTestBuilder;
}());
exports.default = ShippingRateTestBuilder;
