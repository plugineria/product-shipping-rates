"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var ProductMinimalShippingRateQuery_1 = __importDefault(require("../../../App/Query/ProductMinimalShippingRateQuery"));
var ProductShippingRatesQuery_1 = require("../../../App/Query/ProductShippingRatesQuery");
var SessionShippingAddressQuery_1 = require("../../../App/Query/SessionShippingAddressQuery");
var SetSessionShippingAddressUseCase_1 = require("../../../App/UseCase/SetSessionShippingAddress/SetSessionShippingAddressUseCase");
var MockedCustomerSessionResolver_1 = require("../../Mock/Service/MockedCustomerSessionResolver");
var MockedShippingRateResolver_1 = require("../../Mock/Service/ShippingRate/MockedShippingRateResolver");
var MockedAvailableShippingMethodsResolver_1 = require("../../Mock/Service/MockedAvailableShippingMethodsResolver");
var MockedCustomerShippingAddressResolver_1 = require("../../Mock/Service/ShippingAddress/MockedCustomerShippingAddressResolver");
var InMemoryShippingMethodRepository_1 = require("../../InMemory/Repository/InMemoryShippingMethodRepository");
var InMemorySessionShippingAddressRepository_1 = require("../../InMemory/Repository/InMemorySessionShippingAddressRepository");
var InMemoryCustomerShippingAddressRepository_1 = require("../../InMemory/Repository/InMemoryCustomerShippingAddressRepository");
var MockedPostalCodeAddressResolver_1 = require("../../Mock/Service/MockedPostalCodeAddressResolver");
var MockedExampleShippingAddressResolver_1 = require("../../Mock/Service/ShippingAddress/MockedExampleShippingAddressResolver");
var ShippingAddressResolver_1 = require("../../../Domain/Service/ShippingAddress/ShippingAddressResolver");
var ShippingRateViewFactory_1 = require("../../../App/View/ShippingRateViewFactory");
var ProductMinimalShippingRateResolver_1 = require("../../../Domain/Service/ShippingRate/ProductMinimalShippingRateResolver");
var ProductShippingRatesResolver_1 = require("../../../Domain/Service/ShippingRate/ProductShippingRatesResolver");
var SessionCustomerShippingAddressUpdater_1 = require("../../../Domain/Service/ShippingAddress/SessionCustomerShippingAddressUpdater");
var SessionPostalCodeAddressUpdater_1 = require("../../../Domain/Service/ShippingAddress/SessionPostalCodeAddressUpdater");
var TestApp = /** @class */ (function () {
    function TestApp() {
        this.customerSessionResolver = new MockedCustomerSessionResolver_1.MockedCustomerSessionResolver();
        this.shippingRateResolver = new MockedShippingRateResolver_1.MockedShippingRateResolver();
        this.shippingMethodRepository = new InMemoryShippingMethodRepository_1.InMemoryShippingMethodRepository();
        this.availableShippingMethodsResolver = new MockedAvailableShippingMethodsResolver_1.MockedAvailableShippingMethodsResolver(this.shippingMethodRepository);
        this.customerShippingAddressResolver = new MockedCustomerShippingAddressResolver_1.MockedCustomerShippingAddressResolver();
        this.sessionShippingAddressRepository = new InMemorySessionShippingAddressRepository_1.InMemorySessionShippingAddressRepository();
        this.customerShippingAddressRepository = new InMemoryCustomerShippingAddressRepository_1.InMemoryCustomerShippingAddressRepository();
        this.postalCodeAddressResolver = new MockedPostalCodeAddressResolver_1.MockedPostalCodeAddressResolver();
        this.exampleShippingAddressResolver = new MockedExampleShippingAddressResolver_1.MockedExampleShippingAddressResolver();
        var shippingAddressResolver = new ShippingAddressResolver_1.ShippingAddressResolver(this.sessionShippingAddressRepository, this.customerShippingAddressResolver, this.customerSessionResolver, this.exampleShippingAddressResolver);
        var shippingRateViewFactory = new ShippingRateViewFactory_1.ShippingRateViewFactory(this.shippingMethodRepository);
        var productShippingRatesResolver = new ProductShippingRatesResolver_1.ProductShippingRatesResolver(this.availableShippingMethodsResolver, this.shippingRateResolver);
        this.productMinimalShippingRateQuery = new ProductMinimalShippingRateQuery_1.default(new ProductMinimalShippingRateResolver_1.ProductMinimalShippingRateResolver(productShippingRatesResolver), shippingAddressResolver, shippingRateViewFactory);
        this.productShippingRatesQuery = new ProductShippingRatesQuery_1.ProductShippingRatesQuery(productShippingRatesResolver, shippingAddressResolver, shippingRateViewFactory);
        this.setSessionShippingAddressUseCase = new SetSessionShippingAddressUseCase_1.SetSessionShippingAddressUseCase(new SessionCustomerShippingAddressUpdater_1.SessionCustomerShippingAddressUpdater(this.sessionShippingAddressRepository, this.customerShippingAddressRepository, this.customerSessionResolver), new SessionPostalCodeAddressUpdater_1.SessionPostalCodeAddressUpdater(this.sessionShippingAddressRepository, this.postalCodeAddressResolver, this.exampleShippingAddressResolver));
        this.sessionShippingAddressQuery = new SessionShippingAddressQuery_1.SessionShippingAddressQuery(shippingAddressResolver, this.customerShippingAddressRepository, this.customerSessionResolver, this.exampleShippingAddressResolver);
    }
    return TestApp;
}());
exports.default = TestApp;
