import ProductMinimalShippingRateQuery from "../../../App/Query/ProductMinimalShippingRateQuery";
import { ProductShippingRatesQuery } from "../../../App/Query/ProductShippingRatesQuery";
import { SessionShippingAddressQuery } from "../../../App/Query/SessionShippingAddressQuery";
import { SetSessionShippingAddressUseCase } from "../../../App/UseCase/SetSessionShippingAddress/SetSessionShippingAddressUseCase";
import { MockedCustomerSessionResolver } from "../../Mock/Service/MockedCustomerSessionResolver";
import { MockedShippingRateResolver } from "../../Mock/Service/ShippingRate/MockedShippingRateResolver";
import { MockedAvailableShippingMethodsResolver } from "../../Mock/Service/MockedAvailableShippingMethodsResolver";
import { MockedCustomerShippingAddressResolver } from "../../Mock/Service/ShippingAddress/MockedCustomerShippingAddressResolver";
import { InMemoryShippingMethodRepository } from "../../InMemory/Repository/InMemoryShippingMethodRepository";
import { InMemorySessionShippingAddressRepository } from "../../InMemory/Repository/InMemorySessionShippingAddressRepository";
import { InMemoryCustomerShippingAddressRepository } from "../../InMemory/Repository/InMemoryCustomerShippingAddressRepository";
import { MockedPostalCodeAddressResolver } from "../../Mock/Service/MockedPostalCodeAddressResolver";
import { MockedExampleShippingAddressResolver } from "../../Mock/Service/ShippingAddress/MockedExampleShippingAddressResolver";
export default class TestApp {
    readonly productMinimalShippingRateQuery: ProductMinimalShippingRateQuery;
    readonly productShippingRatesQuery: ProductShippingRatesQuery;
    readonly sessionShippingAddressQuery: SessionShippingAddressQuery;
    readonly setSessionShippingAddressUseCase: SetSessionShippingAddressUseCase;
    readonly customerSessionResolver: MockedCustomerSessionResolver;
    readonly shippingRateResolver: MockedShippingRateResolver;
    readonly availableShippingMethodsResolver: MockedAvailableShippingMethodsResolver;
    readonly customerShippingAddressResolver: MockedCustomerShippingAddressResolver;
    readonly shippingMethodRepository: InMemoryShippingMethodRepository;
    readonly sessionShippingAddressRepository: InMemorySessionShippingAddressRepository;
    readonly customerShippingAddressRepository: InMemoryCustomerShippingAddressRepository;
    readonly postalCodeAddressResolver: MockedPostalCodeAddressResolver;
    readonly exampleShippingAddressResolver: MockedExampleShippingAddressResolver;
    constructor();
}
