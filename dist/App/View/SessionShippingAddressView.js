"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SessionShippingAddressView = void 0;
var SessionShippingAddressView = /** @class */ (function () {
    function SessionShippingAddressView(address, customerShippingAddressId, customerShippingAddresses, isProvidedByCustomer) {
        this.address = address;
        this.customerShippingAddressId = customerShippingAddressId;
        this.customerShippingAddresses = customerShippingAddresses;
        this.isProvidedByCustomer = isProvidedByCustomer;
    }
    return SessionShippingAddressView;
}());
exports.SessionShippingAddressView = SessionShippingAddressView;
