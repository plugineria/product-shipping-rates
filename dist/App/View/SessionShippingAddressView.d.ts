import { AddressInterface } from "../../Domain/Model/Address/AddressInterface";
import { CustomerShippingAddressInterface } from "../../Domain/Model/Customer/ShippingAddress/CustomerShippingAddressInterface";
import { CustomerShippingAddressIdInterface } from "../../Domain/Model/Customer/ShippingAddress/CustomerShippingAddressIdInterface";
export declare class SessionShippingAddressView {
    readonly address: AddressInterface;
    readonly customerShippingAddressId: CustomerShippingAddressIdInterface | null;
    readonly customerShippingAddresses: CustomerShippingAddressInterface[];
    readonly isProvidedByCustomer: boolean;
    constructor(address: AddressInterface, customerShippingAddressId: CustomerShippingAddressIdInterface | null, customerShippingAddresses: CustomerShippingAddressInterface[], isProvidedByCustomer: boolean);
}
