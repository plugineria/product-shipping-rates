import { ShippingMethodInterface } from "../../Domain/Model/ShippingMethod/ShippingMethodInterface";
export declare class ShippingRateView {
    readonly shippingMethod: ShippingMethodInterface;
    readonly price: number;
    readonly code: string;
    readonly title?: string | null | undefined;
    readonly description?: string | null | undefined;
    constructor(shippingMethod: ShippingMethodInterface, price: number, code: string, title?: string | null | undefined, description?: string | null | undefined);
}
