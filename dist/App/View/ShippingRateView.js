"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShippingRateView = void 0;
var ShippingRateView = /** @class */ (function () {
    function ShippingRateView(shippingMethod, price, code, title, description) {
        this.shippingMethod = shippingMethod;
        this.price = price;
        this.code = code;
        this.title = title;
        this.description = description;
    }
    return ShippingRateView;
}());
exports.ShippingRateView = ShippingRateView;
