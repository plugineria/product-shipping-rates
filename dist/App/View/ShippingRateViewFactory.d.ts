import { ShippingMethodRepositoryInterface } from "../../Domain/Repository/ShippingMethodRepositoryInterface";
import { ShippingRateInterface } from "../../Domain/Model/ShippingRate/ShippingRateInterface";
import { ShippingRateView } from "./ShippingRateView";
export declare class ShippingRateViewFactory {
    private shippingMethodRepository;
    constructor(shippingMethodRepository: ShippingMethodRepositoryInterface);
    create(shippingRate: ShippingRateInterface): Promise<ShippingRateView>;
}
