export interface CacheInterface {
    get(key: string): Promise<string | null>;
    set(key: string, value: string, ttlSec: number | null): Promise<void>;
}
