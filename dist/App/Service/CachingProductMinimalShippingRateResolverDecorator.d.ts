import ProductMinimalShippingRateResolverInterface from "../../Domain/Service/ShippingRate/ProductMinimalShippingRateResolverInterface";
import { CacheInterface } from "./CacheInterface";
import { ProductIdInterface } from "../../Domain/Model/Product/ProductIdInterface";
import { AddressInterface } from "../../Domain/Model/Address/AddressInterface";
import { ShippingRateInterface } from "../../Domain/Model/ShippingRate/ShippingRateInterface";
import { ShippingRateFactoryInterface } from "../../Domain/Model/ShippingRate/ShippingRateFactoryInterface";
export declare class CachingProductMinimalShippingRateResolverDecorator implements ProductMinimalShippingRateResolverInterface {
    private realResolver;
    private cache;
    private ttl;
    private shippingRateFactory;
    private static readonly CACHE_KEY_PREFIX;
    constructor(realResolver: ProductMinimalShippingRateResolverInterface, cache: CacheInterface, ttl: number, shippingRateFactory: ShippingRateFactoryInterface);
    getMinimalShippingRate(productId: ProductIdInterface, shippingAddress: AddressInterface): Promise<ShippingRateInterface | null>;
    private static getCacheKey;
}
