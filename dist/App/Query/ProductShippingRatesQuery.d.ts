import ProductShippingRatesQueryPort from "./ProductShippingRatesQueryPort";
import { ProductIdInterface } from "../../Domain/Model/Product/ProductIdInterface";
import { ShippingRateView } from "../View/ShippingRateView";
import { ProductShippingRatesResolver } from "../../Domain/Service/ShippingRate/ProductShippingRatesResolver";
import { ShippingAddressResolverInterface } from "../../Domain/Service/ShippingAddress/ShippingAddressResolverInterface";
import { ShippingRateViewFactory } from "../View/ShippingRateViewFactory";
export declare class ProductShippingRatesQuery implements ProductShippingRatesQueryPort {
    private productShippingRatesResolver;
    private shippingAddressResolver;
    private shippingRateViewFactory;
    constructor(productShippingRatesResolver: ProductShippingRatesResolver, shippingAddressResolver: ShippingAddressResolverInterface, shippingRateViewFactory: ShippingRateViewFactory);
    execute(productId: ProductIdInterface): Promise<ShippingRateView[]>;
}
