import { ProductIdInterface } from "../../Domain/Model/Product/ProductIdInterface";
import { ShippingRateView } from "../View/ShippingRateView";
export default interface ProductShippingRatesQueryPort {
    execute(productId: ProductIdInterface): Promise<ShippingRateView[]>;
}
