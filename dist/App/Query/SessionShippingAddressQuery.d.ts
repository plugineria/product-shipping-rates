import { SessionShippingAddressQueryPort } from "./SessionShippingAddressQueryPort";
import { SessionShippingAddressView } from "../View/SessionShippingAddressView";
import { ShippingAddressResolverInterface } from "../../Domain/Service/ShippingAddress/ShippingAddressResolverInterface";
import CustomerShippingAddressRepositoryInterface from "../../Domain/Repository/CustomerShippingAddressRepositoryInterface";
import { CustomerSessionResolverInterface } from "../../Domain/Service/CustomerSessionResolverInterface";
import { ExampleShippingAddressResolverInterface } from "../../Domain/Service/ShippingAddress/ExampleShippingAddressResolverInterface";
export declare class SessionShippingAddressQuery implements SessionShippingAddressQueryPort {
    private shippingAddressResolver;
    private customerShippingAddressRepository;
    private customerSessionResolver;
    private exampleShippingAddressResolver;
    constructor(shippingAddressResolver: ShippingAddressResolverInterface, customerShippingAddressRepository: CustomerShippingAddressRepositoryInterface, customerSessionResolver: CustomerSessionResolverInterface, exampleShippingAddressResolver: ExampleShippingAddressResolverInterface);
    execute(): Promise<SessionShippingAddressView>;
    private createSessionShippingAddressView;
}
