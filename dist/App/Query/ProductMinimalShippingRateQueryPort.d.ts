import { ProductIdInterface } from "../../Domain/Model/Product/ProductIdInterface";
import { ShippingRateView } from "../View/ShippingRateView";
export default interface ProductMinimalShippingRateQueryPort {
    execute(productId: ProductIdInterface): Promise<ShippingRateView | null>;
}
