import { ShippingRateView } from "../View/ShippingRateView";
import { ProductIdInterface } from "../../Domain/Model/Product/ProductIdInterface";
import ProductMinimalShippingRateQueryPort from "./ProductMinimalShippingRateQueryPort";
import ProductMinimalShippingRateResolverInterface from "../../Domain/Service/ShippingRate/ProductMinimalShippingRateResolverInterface";
import { ShippingAddressResolverInterface } from "../../Domain/Service/ShippingAddress/ShippingAddressResolverInterface";
import { ShippingRateViewFactory } from "../View/ShippingRateViewFactory";
export default class ProductMinimalShippingRateQuery implements ProductMinimalShippingRateQueryPort {
    private productMinimalShippingRateResolver;
    private shippingAddressResolver;
    private shippingRateViewFactory;
    constructor(productMinimalShippingRateResolver: ProductMinimalShippingRateResolverInterface, shippingAddressResolver: ShippingAddressResolverInterface, shippingRateViewFactory: ShippingRateViewFactory);
    execute(productId: ProductIdInterface): Promise<ShippingRateView | null>;
}
