import { SessionShippingAddressView } from "../View/SessionShippingAddressView";
export interface SessionShippingAddressQueryPort {
    execute(): Promise<SessionShippingAddressView>;
}
