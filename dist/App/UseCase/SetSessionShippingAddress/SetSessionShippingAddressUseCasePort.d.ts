import { SetSessionShippingAddressCommand } from "./SetSessionShippingAddressCommand";
import { SetSessionShippingAddressResult } from "./SetSessionShippingAddressResult";
export interface SetSessionShippingAddressUseCasePort {
    execute(command: SetSessionShippingAddressCommand): Promise<SetSessionShippingAddressResult>;
}
