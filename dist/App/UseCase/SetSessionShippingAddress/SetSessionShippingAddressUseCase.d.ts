import { SetSessionShippingAddressUseCasePort } from "./SetSessionShippingAddressUseCasePort";
import { SetSessionShippingAddressCommand } from "./SetSessionShippingAddressCommand";
import { SetSessionShippingAddressResult } from "./SetSessionShippingAddressResult";
import { SessionCustomerShippingAddressUpdater } from "../../../Domain/Service/ShippingAddress/SessionCustomerShippingAddressUpdater";
import { SessionPostalCodeAddressUpdater } from "../../../Domain/Service/ShippingAddress/SessionPostalCodeAddressUpdater";
export declare class SetSessionShippingAddressUseCase implements SetSessionShippingAddressUseCasePort {
    private customerShippingAddressUpdater;
    private postalCodeUpdater;
    constructor(customerShippingAddressUpdater: SessionCustomerShippingAddressUpdater, postalCodeUpdater: SessionPostalCodeAddressUpdater);
    execute(command: SetSessionShippingAddressCommand): Promise<SetSessionShippingAddressResult>;
}
