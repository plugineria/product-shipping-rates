"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SetSessionShippingAddressCommand = void 0;
var SetSessionShippingAddressCommand = /** @class */ (function () {
    function SetSessionShippingAddressCommand(country, postalCode, customerShippingAddressId) {
        if (country === void 0) { country = null; }
        if (postalCode === void 0) { postalCode = null; }
        if (customerShippingAddressId === void 0) { customerShippingAddressId = null; }
        this.country = country;
        this.postalCode = postalCode;
        this.customerShippingAddressId = customerShippingAddressId;
    }
    return SetSessionShippingAddressCommand;
}());
exports.SetSessionShippingAddressCommand = SetSessionShippingAddressCommand;
