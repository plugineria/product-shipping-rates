"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.SetSessionShippingAddressResult = void 0;
var SetSessionShippingAddressResult = /** @class */ (function () {
    function SetSessionShippingAddressResult(isValid, error) {
        if (isValid === void 0) { isValid = true; }
        if (error === void 0) { error = null; }
        this.isValid = isValid;
        this.error = error;
    }
    SetSessionShippingAddressResult.createValid = function () {
        return new SetSessionShippingAddressResult();
    };
    SetSessionShippingAddressResult.createInvalidWithAddressNotSpecified = function () {
        return new SetSessionShippingAddressResult(false, SetSessionShippingAddressResult.ADDRESS_NOT_SPECIFIED);
    };
    SetSessionShippingAddressResult.createInvalidWithCustomerAddressIdNotFound = function () {
        return new SetSessionShippingAddressResult(false, SetSessionShippingAddressResult.CUSTOMER_ADDRESS_ID_NOT_FOUND);
    };
    SetSessionShippingAddressResult.createInvalidWithPostalCodeDoesNotExistInCountry = function () {
        return new SetSessionShippingAddressResult(false, SetSessionShippingAddressResult.POSTAL_CODE_DOES_NOT_EXIST_IN_COUNTRY);
    };
    SetSessionShippingAddressResult.ADDRESS_NOT_SPECIFIED = 'address_not_specified';
    SetSessionShippingAddressResult.CUSTOMER_ADDRESS_ID_NOT_FOUND = 'customer_address_id_not_found';
    SetSessionShippingAddressResult.POSTAL_CODE_DOES_NOT_EXIST_IN_COUNTRY = 'postal_code_does_not_exist_in_country';
    return SetSessionShippingAddressResult;
}());
exports.SetSessionShippingAddressResult = SetSessionShippingAddressResult;
