export declare class SetSessionShippingAddressResult {
    readonly isValid: boolean;
    readonly error: string | null;
    static readonly ADDRESS_NOT_SPECIFIED = "address_not_specified";
    static readonly CUSTOMER_ADDRESS_ID_NOT_FOUND = "customer_address_id_not_found";
    static readonly POSTAL_CODE_DOES_NOT_EXIST_IN_COUNTRY = "postal_code_does_not_exist_in_country";
    private constructor();
    static createValid(): SetSessionShippingAddressResult;
    static createInvalidWithAddressNotSpecified(): SetSessionShippingAddressResult;
    static createInvalidWithCustomerAddressIdNotFound(): SetSessionShippingAddressResult;
    static createInvalidWithPostalCodeDoesNotExistInCountry(): SetSessionShippingAddressResult;
}
