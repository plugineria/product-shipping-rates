import { CustomerShippingAddressIdInterface } from "../../../Domain/Model/Customer/ShippingAddress/CustomerShippingAddressIdInterface";
export declare class SetSessionShippingAddressCommand {
    readonly country: string | null;
    readonly postalCode: string | null;
    readonly customerShippingAddressId: CustomerShippingAddressIdInterface | null;
    constructor(country?: string | null, postalCode?: string | null, customerShippingAddressId?: CustomerShippingAddressIdInterface | null);
}
