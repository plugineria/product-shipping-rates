import CustomerShippingAddressRepositoryInterface from "../../Repository/CustomerShippingAddressRepositoryInterface";
import { CustomerShippingAddressIdInterface } from "../../Model/Customer/ShippingAddress/CustomerShippingAddressIdInterface";
import SessionShippingAddressRepositoryInterface from "../../Repository/SessionShippingAddressRepositoryInterface";
import { CustomerSessionResolverInterface } from "../CustomerSessionResolverInterface";
export declare class SessionCustomerShippingAddressUpdater {
    private sessionShippingAddressRepository;
    private customerShippingAddressRepository;
    private customerSessionResolver;
    constructor(sessionShippingAddressRepository: SessionShippingAddressRepositoryInterface, customerShippingAddressRepository: CustomerShippingAddressRepositoryInterface, customerSessionResolver: CustomerSessionResolverInterface);
    /**
     * @param {CustomerShippingAddressIdInterface} addressId
     * @throws {CustomerShippingAddressNotFound}
     */
    update(addressId: CustomerShippingAddressIdInterface): Promise<void>;
}
