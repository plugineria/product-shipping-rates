import { AddressInterface } from "../../Model/Address/AddressInterface";
export interface ShippingAddressResolverInterface {
    getDefaultShippingAddress(): Promise<AddressInterface>;
}
