import SessionShippingAddressRepositoryInterface from "../../Repository/SessionShippingAddressRepositoryInterface";
import { PostalCodeAddressResolverInterface } from "../PostalCodeAddressResolverInterface";
import { ExampleShippingAddressResolverInterface } from "./ExampleShippingAddressResolverInterface";
export declare class SessionPostalCodeAddressUpdater {
    private sessionShippingAddressRepository;
    private postalCodeAddressResolver;
    private exampleShippingAddressResolver;
    constructor(sessionShippingAddressRepository: SessionShippingAddressRepositoryInterface, postalCodeAddressResolver: PostalCodeAddressResolverInterface, exampleShippingAddressResolver: ExampleShippingAddressResolverInterface);
    /**
     * @param {string} postalCode
     * @param {string|null} country
     * @throws {PostalCodeAddressNotFound}
     */
    update(postalCode: string, country?: string | null): Promise<void>;
}
