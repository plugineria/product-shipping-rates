import { AddressInterface } from "../../Model/Address/AddressInterface";
export interface ExampleShippingAddressResolverInterface {
    /**
     * @throws {ExampleShippingAddressNotDefined}
     */
    getExampleShippingAddress(): Promise<AddressInterface>;
}
