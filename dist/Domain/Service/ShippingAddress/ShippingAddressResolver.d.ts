import { ShippingAddressResolverInterface } from "./ShippingAddressResolverInterface";
import { AddressInterface } from "../../Model/Address/AddressInterface";
import SessionShippingAddressRepositoryInterface from "../../Repository/SessionShippingAddressRepositoryInterface";
import { CustomerShippingAddressResolverInterface } from "./CustomerShippingAddressResolverInterface";
import { CustomerSessionResolverInterface } from "../CustomerSessionResolverInterface";
import { ExampleShippingAddressResolverInterface } from "./ExampleShippingAddressResolverInterface";
export declare class ShippingAddressResolver implements ShippingAddressResolverInterface {
    private sessionShippingAddressRepository;
    private customerShippingAddressResolver;
    private customerSessionResolver;
    private exampleShippingAddressResolver;
    constructor(sessionShippingAddressRepository: SessionShippingAddressRepositoryInterface, customerShippingAddressResolver: CustomerShippingAddressResolverInterface, customerSessionResolver: CustomerSessionResolverInterface, exampleShippingAddressResolver: ExampleShippingAddressResolverInterface);
    getDefaultShippingAddress(): Promise<AddressInterface>;
}
