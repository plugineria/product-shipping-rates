import { CustomerShippingAddressInterface } from "../../Model/Customer/ShippingAddress/CustomerShippingAddressInterface";
import { CustomerIdInterface } from "../../Model/Customer/CustomerIdInterface";
export interface CustomerShippingAddressResolverInterface {
    getPrimaryCustomerShippingAddress(customerId: CustomerIdInterface): Promise<CustomerShippingAddressInterface | null>;
}
