import AvailableShippingMethodsResolverInterface from "../AvailableShippingMethodsResolverInterface";
import { ShippingRateResolverInterface } from "./ShippingRateResolverInterface";
import { ShippingRateInterface } from "../../Model/ShippingRate/ShippingRateInterface";
import { ProductIdInterface } from "../../Model/Product/ProductIdInterface";
import { AddressInterface } from "../../Model/Address/AddressInterface";
export declare class ProductShippingRatesResolver {
    private availableShippingMethodsResolver;
    private shippingRateResolver;
    constructor(availableShippingMethodsResolver: AvailableShippingMethodsResolverInterface, shippingRateResolver: ShippingRateResolverInterface);
    getProductShippingRates(productId: ProductIdInterface, shippingAddress: AddressInterface): Promise<ShippingRateInterface[]>;
}
