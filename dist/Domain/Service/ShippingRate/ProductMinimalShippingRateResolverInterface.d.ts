import { ShippingRateInterface } from "../../Model/ShippingRate/ShippingRateInterface";
import { ProductIdInterface } from "../../Model/Product/ProductIdInterface";
import { AddressInterface } from "../../Model/Address/AddressInterface";
export default interface ProductMinimalShippingRateResolverInterface {
    getMinimalShippingRate(productId: ProductIdInterface, shippingAddress: AddressInterface): Promise<ShippingRateInterface | null>;
}
