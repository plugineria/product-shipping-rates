import ProductMinimalShippingRateResolverInterface from "./ProductMinimalShippingRateResolverInterface";
import { ProductIdInterface } from "../../Model/Product/ProductIdInterface";
import { AddressInterface } from "../../Model/Address/AddressInterface";
import { ShippingRateInterface } from "../../Model/ShippingRate/ShippingRateInterface";
import { ProductShippingRatesResolver } from "./ProductShippingRatesResolver";
export declare class ProductMinimalShippingRateResolver implements ProductMinimalShippingRateResolverInterface {
    private productShippingRatesResolver;
    constructor(productShippingRatesResolver: ProductShippingRatesResolver);
    getMinimalShippingRate(productId: ProductIdInterface, shippingAddress: AddressInterface): Promise<ShippingRateInterface | null>;
}
