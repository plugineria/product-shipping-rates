import { ShippingMethodIdInterface } from "../Model/ShippingMethod/ShippingMethodIdInterface";
import { ShippingMethodInterface } from "../Model/ShippingMethod/ShippingMethodInterface";
export interface ShippingMethodRepositoryInterface {
    /**
     * @param {ShippingMethodIdInterface} id
     * @throws {ShippingMethodNotFound}
     */
    getById(id: ShippingMethodIdInterface): Promise<ShippingMethodInterface>;
    findById(id: ShippingMethodIdInterface): Promise<ShippingMethodInterface | null>;
}
