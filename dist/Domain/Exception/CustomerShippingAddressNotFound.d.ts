import { CustomerShippingAddressIdInterface } from "../Model/Customer/ShippingAddress/CustomerShippingAddressIdInterface";
export declare class CustomerShippingAddressNotFound extends Error {
    readonly id: CustomerShippingAddressIdInterface;
    constructor(id: CustomerShippingAddressIdInterface);
}
