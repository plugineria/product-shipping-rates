"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomerShippingAddressNotFound = void 0;
var CustomerShippingAddressNotFound = /** @class */ (function (_super) {
    __extends(CustomerShippingAddressNotFound, _super);
    function CustomerShippingAddressNotFound(id) {
        var _this = _super.call(this, "Customer shipping address " + id + " not found") || this;
        _this.id = id;
        Object.setPrototypeOf(_this, CustomerShippingAddressNotFound.prototype);
        return _this;
    }
    return CustomerShippingAddressNotFound;
}(Error));
exports.CustomerShippingAddressNotFound = CustomerShippingAddressNotFound;
