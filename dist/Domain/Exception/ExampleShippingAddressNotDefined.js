"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExampleShippingAddressNotDefined = void 0;
var ExampleShippingAddressNotDefined = /** @class */ (function (_super) {
    __extends(ExampleShippingAddressNotDefined, _super);
    function ExampleShippingAddressNotDefined() {
        var _this = _super.call(this, "Example shipping address not defined") || this;
        Object.setPrototypeOf(_this, ExampleShippingAddressNotDefined.prototype);
        return _this;
    }
    return ExampleShippingAddressNotDefined;
}(Error));
exports.ExampleShippingAddressNotDefined = ExampleShippingAddressNotDefined;
