import { ShippingMethodIdInterface } from "../Model/ShippingMethod/ShippingMethodIdInterface";
export declare class ShippingMethodNotFound extends Error {
    readonly id: ShippingMethodIdInterface;
    constructor(id: ShippingMethodIdInterface);
}
