export declare class PostalCodeAddressNotFound extends Error {
    readonly postalCode: string;
    readonly country: string | null;
    constructor(postalCode: string, country: string | null);
}
