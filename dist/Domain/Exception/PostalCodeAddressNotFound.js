"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
exports.PostalCodeAddressNotFound = void 0;
var PostalCodeAddressNotFound = /** @class */ (function (_super) {
    __extends(PostalCodeAddressNotFound, _super);
    function PostalCodeAddressNotFound(postalCode, country) {
        var _this = _super.call(this, "Address for postal code: " + postalCode + " in country " + country + " not found") || this;
        _this.postalCode = postalCode;
        _this.country = country;
        Object.setPrototypeOf(_this, PostalCodeAddressNotFound.prototype);
        return _this;
    }
    return PostalCodeAddressNotFound;
}(Error));
exports.PostalCodeAddressNotFound = PostalCodeAddressNotFound;
