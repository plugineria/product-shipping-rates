export declare class AddressAttributeNotFound extends Error {
    readonly attributeName: string;
    constructor(attributeName: string);
}
