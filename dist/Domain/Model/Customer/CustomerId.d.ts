import { CustomerIdInterface } from "./CustomerIdInterface";
export declare class CustomerId implements CustomerIdInterface {
    readonly id: string;
    constructor(id: string);
    toString(): string;
}
