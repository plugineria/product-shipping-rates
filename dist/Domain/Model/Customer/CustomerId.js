"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomerId = void 0;
var CustomerId = /** @class */ (function () {
    function CustomerId(id) {
        this.id = id;
    }
    CustomerId.prototype.toString = function () {
        return this.id;
    };
    return CustomerId;
}());
exports.CustomerId = CustomerId;
