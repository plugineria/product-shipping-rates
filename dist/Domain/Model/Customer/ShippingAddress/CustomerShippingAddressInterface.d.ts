import { CustomerShippingAddressIdInterface } from "./CustomerShippingAddressIdInterface";
import { CustomerIdInterface } from "../CustomerIdInterface";
import { AddressInterface } from "../../Address/AddressInterface";
export interface CustomerShippingAddressInterface {
    readonly id: CustomerShippingAddressIdInterface;
    readonly customerId: CustomerIdInterface;
    readonly address: AddressInterface;
}
