export interface CustomerShippingAddressIdInterface {
    toString(): string;
}
