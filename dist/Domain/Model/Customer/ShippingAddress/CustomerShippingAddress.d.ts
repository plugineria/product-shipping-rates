import { CustomerShippingAddressInterface } from "./CustomerShippingAddressInterface";
import { CustomerShippingAddressIdInterface } from "./CustomerShippingAddressIdInterface";
import { CustomerIdInterface } from "../CustomerIdInterface";
import { AddressInterface } from "../../Address/AddressInterface";
export declare class CustomerShippingAddress implements CustomerShippingAddressInterface {
    readonly id: CustomerShippingAddressIdInterface;
    readonly customerId: CustomerIdInterface;
    readonly address: AddressInterface;
    constructor(id: CustomerShippingAddressIdInterface, customerId: CustomerIdInterface, address: AddressInterface);
}
