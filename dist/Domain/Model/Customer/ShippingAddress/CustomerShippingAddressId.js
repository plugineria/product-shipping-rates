"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomerShippingAddressId = void 0;
var CustomerShippingAddressId = /** @class */ (function () {
    function CustomerShippingAddressId(id) {
        this.id = id;
    }
    CustomerShippingAddressId.prototype.toString = function () {
        return this.id;
    };
    return CustomerShippingAddressId;
}());
exports.CustomerShippingAddressId = CustomerShippingAddressId;
