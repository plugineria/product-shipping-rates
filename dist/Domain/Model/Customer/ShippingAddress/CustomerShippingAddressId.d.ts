import { CustomerShippingAddressIdInterface } from "./CustomerShippingAddressIdInterface";
export declare class CustomerShippingAddressId implements CustomerShippingAddressIdInterface {
    readonly id: string;
    constructor(id: string);
    toString(): string;
}
