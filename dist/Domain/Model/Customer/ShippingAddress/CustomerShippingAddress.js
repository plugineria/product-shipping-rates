"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomerShippingAddress = void 0;
var CustomerShippingAddress = /** @class */ (function () {
    function CustomerShippingAddress(id, customerId, address) {
        this.id = id;
        this.customerId = customerId;
        this.address = address;
    }
    return CustomerShippingAddress;
}());
exports.CustomerShippingAddress = CustomerShippingAddress;
