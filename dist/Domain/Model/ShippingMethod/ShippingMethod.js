"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShippingMethod = void 0;
var ShippingMethod = /** @class */ (function () {
    function ShippingMethod(id, title) {
        this.id = id;
        this.title = title;
    }
    return ShippingMethod;
}());
exports.ShippingMethod = ShippingMethod;
