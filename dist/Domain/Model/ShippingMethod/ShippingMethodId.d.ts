import { ShippingMethodIdInterface } from "./ShippingMethodIdInterface";
export declare class ShippingMethodId implements ShippingMethodIdInterface {
    readonly id: string;
    constructor(id: string);
    toString(): string;
}
