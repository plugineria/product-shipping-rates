"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShippingMethodId = void 0;
var ShippingMethodId = /** @class */ (function () {
    function ShippingMethodId(id) {
        this.id = id;
    }
    ShippingMethodId.prototype.toString = function () {
        return this.id;
    };
    return ShippingMethodId;
}());
exports.ShippingMethodId = ShippingMethodId;
