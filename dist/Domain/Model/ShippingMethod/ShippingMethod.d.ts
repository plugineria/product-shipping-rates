import { ShippingMethodInterface } from "./ShippingMethodInterface";
import { ShippingMethodIdInterface } from "./ShippingMethodIdInterface";
export declare class ShippingMethod implements ShippingMethodInterface {
    readonly id: ShippingMethodIdInterface;
    readonly title: string;
    constructor(id: ShippingMethodIdInterface, title: string);
}
