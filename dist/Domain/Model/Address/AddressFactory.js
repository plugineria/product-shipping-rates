"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AddressFactory = void 0;
var Address_1 = require("./Address");
var AddressAttributeNotFound_1 = require("../../Exception/AddressAttributeNotFound");
var AddressFactory = /** @class */ (function () {
    function AddressFactory() {
    }
    AddressFactory.prototype.createFromString = function (addressJson) {
        var address = JSON.parse(addressJson);
        if (address.country === undefined) {
            throw new AddressAttributeNotFound_1.AddressAttributeNotFound("country");
        }
        if (address.city === undefined) {
            throw new AddressAttributeNotFound_1.AddressAttributeNotFound("city");
        }
        return new Address_1.Address(address.country, address.city, "postalCode" in address ? address.postalCode : null, "street" in address ? address.street : null, "region" in address ? address.region : null);
    };
    return AddressFactory;
}());
exports.AddressFactory = AddressFactory;
