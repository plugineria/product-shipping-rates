import { AddressInterface } from "./AddressInterface";
export declare class Address implements AddressInterface {
    readonly country: string;
    readonly city: string;
    readonly postalCode: string | null;
    readonly street: string | null;
    readonly region: string | null;
    constructor(country: string, city: string, postalCode?: string | null, street?: string | null, region?: string | null);
    equals(address: AddressInterface): boolean;
    toString(): string;
}
