"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Address = void 0;
var Address = /** @class */ (function () {
    function Address(country, city, postalCode, street, region) {
        if (postalCode === void 0) { postalCode = null; }
        if (street === void 0) { street = null; }
        if (region === void 0) { region = null; }
        this.country = country;
        this.city = city;
        this.postalCode = postalCode;
        this.street = street;
        this.region = region;
    }
    Address.prototype.equals = function (address) {
        return this.toString() === address.toString();
    };
    Address.prototype.toString = function () {
        return JSON.stringify({
            "country": this.country,
            "region": this.region,
            "postalCode": this.postalCode,
            "city": this.city,
            "street": this.street,
        });
    };
    return Address;
}());
exports.Address = Address;
