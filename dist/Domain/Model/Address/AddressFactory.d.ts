import { AddressFactoryInterface } from "./AddressFactoryInterface";
import { Address } from "./Address";
export declare class AddressFactory implements AddressFactoryInterface {
    createFromString(addressJson: string): Address;
}
