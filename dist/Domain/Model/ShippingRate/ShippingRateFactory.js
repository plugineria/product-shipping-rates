"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShippingRateFactory = void 0;
var ShippingRate_1 = require("./ShippingRate");
var ShippingMethodId_1 = require("../ShippingMethod/ShippingMethodId");
var ShippingRateFactory = /** @class */ (function () {
    function ShippingRateFactory() {
    }
    ShippingRateFactory.prototype.createFromString = function (shippingRateJson) {
        var shippingRate = JSON.parse(shippingRateJson);
        return new ShippingRate_1.ShippingRate(new ShippingMethodId_1.ShippingMethodId(shippingRate.shippingMethodId), shippingRate.code, shippingRate.price, "title" in shippingRate ? shippingRate.title : null, "description" in shippingRate ? shippingRate.description : null);
    };
    return ShippingRateFactory;
}());
exports.ShippingRateFactory = ShippingRateFactory;
