"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ShippingRate = void 0;
var ShippingRate = /** @class */ (function () {
    function ShippingRate(shippingMethodId, code, price, title, description) {
        if (title === void 0) { title = null; }
        if (description === void 0) { description = null; }
        this.shippingMethodId = shippingMethodId;
        this.code = code;
        this.price = price;
        this.title = title;
        this.description = description;
    }
    ShippingRate.prototype.toString = function () {
        return JSON.stringify({
            "shippingMethodId": this.shippingMethodId.toString(),
            "code": this.code,
            "price": this.price,
            "title": this.title,
            "description": this.description,
        });
    };
    return ShippingRate;
}());
exports.ShippingRate = ShippingRate;
