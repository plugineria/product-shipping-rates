import { ShippingRateFactoryInterface } from "./ShippingRateFactoryInterface";
import { ShippingRate } from "./ShippingRate";
export declare class ShippingRateFactory implements ShippingRateFactoryInterface {
    createFromString(shippingRateJson: string): ShippingRate;
}
