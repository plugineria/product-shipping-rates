import { ShippingRateInterface } from "./ShippingRateInterface";
import { ShippingMethodIdInterface } from "../ShippingMethod/ShippingMethodIdInterface";
export declare class ShippingRate implements ShippingRateInterface {
    readonly shippingMethodId: ShippingMethodIdInterface;
    readonly code: string;
    readonly price: number;
    readonly title: string | null;
    readonly description: string | null;
    constructor(shippingMethodId: ShippingMethodIdInterface, code: string, price: number, title?: string | null, description?: string | null);
    toString(): string;
}
