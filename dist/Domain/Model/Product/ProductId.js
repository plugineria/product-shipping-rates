"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductId = void 0;
var ProductId = /** @class */ (function () {
    function ProductId(id) {
        this.id = id;
    }
    ProductId.prototype.toString = function () {
        return this.id;
    };
    return ProductId;
}());
exports.ProductId = ProductId;
