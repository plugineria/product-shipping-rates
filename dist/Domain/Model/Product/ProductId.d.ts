import { ProductIdInterface } from "./ProductIdInterface";
export declare class ProductId implements ProductIdInterface {
    readonly id: string;
    constructor(id: string);
    toString(): string;
}
