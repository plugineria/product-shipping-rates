import {setWorldConstructor} from "@cucumber/cucumber";
import TestApp from "../../src/Infrastructure/Test/Integration/TestApp";
import {ProductIdInterface} from "../../src/Domain/Model/Product/ProductIdInterface";
import {SetSessionShippingAddressResult} from "../../src/App/UseCase/SetSessionShippingAddress/SetSessionShippingAddressResult";

export class World {
    app = new TestApp();
    selectedProductId: ProductIdInterface|undefined;
    setSessionShippingAddressResult: SetSessionShippingAddressResult|undefined;
}

setWorldConstructor(World);
