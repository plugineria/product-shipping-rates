import {Given, When, Then, DataTable} from '@cucumber/cucumber'
import AddressTestBuilder from "../../src/Infrastructure/Test/Builder/AddressTestBuilder";
import {CustomerShippingAddressId} from "../../src/Domain/Model/Customer/ShippingAddress/CustomerShippingAddressId";
import {SetSessionShippingAddressCommand} from "../../src/App/UseCase/SetSessionShippingAddress/SetSessionShippingAddressCommand";
import expect from 'expect';
import {SetSessionShippingAddressResult} from "../../src/App/UseCase/SetSessionShippingAddress/SetSessionShippingAddressResult";
import {CustomerShippingAddress} from "../../src/Domain/Model/Customer/ShippingAddress/CustomerShippingAddress";
import {CustomerId} from "../../src/Domain/Model/Customer/CustomerId";
// @ts-ignore
import {World} from "../support/world";

Given(
    /^user "([^"]*)" has shipping addresses:$/,
    function (this: World, username: string, tableShippingAddresses: DataTable): void {
        for (const tableShippingAddress of tableShippingAddresses.hashes()) {
            this.app.customerShippingAddressRepository.save(new CustomerShippingAddress(
                new CustomerShippingAddressId(tableShippingAddress.id),
                new CustomerId(username),
                AddressTestBuilder.create()
                    .withCity(tableShippingAddress.city)
                    .withPostalCode(tableShippingAddress.postal_code)
                    .withRegion(tableShippingAddress.region)
                    .withCountry(tableShippingAddress.country)
                    .withStreet(tableShippingAddress.street_address)
                    .build(),
            ))
        }
    }
);

Given(
    /^user "(?<username>[^"]*)" has primary shipping address "(?<addressId>[^"]*)"$/,
    async function (this: World, username: string, addressId: string): Promise<void> {
        this.app.customerShippingAddressResolver.setCustomerPrimaryShippingAddress(
            await this.app.customerShippingAddressRepository.getById(
                new CustomerShippingAddressId(addressId)
            )
        )
    }
);

Given(
    /^example shipping address is "(?<country>[^"]*)", "(?<region>[^"]*)", "(?<postalCode>[^"]*)", "(?<city>[^"]*)", "(?<street>[^"]*)"$/,
    function (
        this: World,
        country: string,
        region: string,
        postalCode: string,
        city: string,
        street: string,
    ): void {
        this.app.exampleShippingAddressResolver.exampleShippingAddress = AddressTestBuilder.create()
            .withCountry(country)
            .withRegion(region)
            .withPostalCode(postalCode)
            .withStreet(street)
            .build();
    }
);

When(
    /^user sets session shipping address to "(?<customerAddressId>[^"]*)"$/,
    async function (this: World, customerAddressId: string): Promise<void> {
        this.setSessionShippingAddressResult = await this.app.setSessionShippingAddressUseCase.execute(
            new SetSessionShippingAddressCommand(
                null,
                null,
                new CustomerShippingAddressId(customerAddressId),
            )
        );
    }
);

Then(
    /^user session address becomes "(?<country>[^"]*)"(, "(?<region>[^"]*)")?, "(?<postalCode>[^"]*)", "(?<city>[^"]*)"(, "(?<street>[^"]*)")?$/,
    async function (
        this: World,
        country: string,
        region: string|undefined = undefined,
        postalCode: string,
        city: string,
        street: string|undefined = undefined,
    ): Promise<void> {
        const sessionShippingAddress = await this.app.sessionShippingAddressQuery.execute();

        expect(sessionShippingAddress.address).toEqual(AddressTestBuilder.create()
            .withCountry(country)
            .withRegion(region || null)
            .withPostalCode(postalCode)
            .withCity(city)
            .withStreet(street || null)
            .build()
        );
    }
);

Then(
    /^such shipping address does not exist$/,
    function (this: World): void {
        expect(this.setSessionShippingAddressResult?.isValid).toBeFalsy();
        expect(this.setSessionShippingAddressResult?.error).toEqual(
            SetSessionShippingAddressResult.CUSTOMER_ADDRESS_ID_NOT_FOUND
        );
    }
);

Given(
    /^postal code "(?<postalCode>[^"]*)" is located in "(?<city>[^"]*)"$/,
    function (this: World, postalCode: string, city: string): void {
        this.app.postalCodeAddressResolver.city = city;
    }
);

Given(
    /^user is not logged in$/,
    function (this: World): void {
        this.app.customerSessionResolver.customerId = null;
    }
);

When(
    /^user sets session shipping postal code to "(?<postalCode>[^"]*)"$/,
    async function (this: World, postalCode: string): Promise<void> {
        this.setSessionShippingAddressResult = await this.app.setSessionShippingAddressUseCase.execute(
            new SetSessionShippingAddressCommand(
                null,
                postalCode,
                null,
            )
        );
    }
);

Then(
    /^such postal code does not exist$/,
    function (this: World): void {
        expect(this.setSessionShippingAddressResult?.isValid).toBeFalsy();
        expect(this.setSessionShippingAddressResult?.error).toEqual(
            SetSessionShippingAddressResult.POSTAL_CODE_DOES_NOT_EXIST_IN_COUNTRY
        )
    }
);
