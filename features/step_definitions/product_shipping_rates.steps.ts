import {DataTable, Given, When, Then} from '@cucumber/cucumber'
import {CustomerId} from "../../src/Domain/Model/Customer/CustomerId";
import {ShippingMethod} from "../../src/Domain/Model/ShippingMethod/ShippingMethod";
import {ShippingMethodId} from "../../src/Domain/Model/ShippingMethod/ShippingMethodId";
import ShippingRateTestBuilder from "../../src/Infrastructure/Test/Builder/ShippingRateTestBuilder";
import {ProductId} from "../../src/Domain/Model/Product/ProductId";
import {ShippingRateView} from "../../src/App/View/ShippingRateView";
import ShippingMethodTestBuilder from "../../src/Infrastructure/Test/Builder/ShippingMethodTestBuilder";
import expect from 'expect';
// @ts-ignore
import {World} from "../support/world";

Given(
    /^user is logged in as "(?<username>[^"]*)"$/,
    function (this: World, username: string) {
        this.app.customerSessionResolver.customerId = new CustomerId(username);
    }
);

Given(
    /^shipping methods are available for "(?<productId>[^"]*)" shipped to "(?<country>[^"]*)", "(?<region>[^"]*)", "(?<postalCode>[^"]*)", "(?<city>[^"]*)", "(?<street>[^"]*)":$/,
    function (
        this: World,
        productId: string,
        country: string,
        region: string,
        postalCode: string,
        city: string,
        street: string,
        shippingMethods: DataTable,
    ): void {
        for (const [shippingMethodId, shippingMethodTitle] of shippingMethods.rows()) {
            const shippingMethod = new ShippingMethod(
                new ShippingMethodId(shippingMethodId),
                shippingMethodTitle,
            );
            this.app.shippingMethodRepository.save(shippingMethod);
        }

        this.app.availableShippingMethodsResolver.shippingMethodIds = shippingMethods.rows().map(
            ([shippingMethodId]) => new ShippingMethodId(shippingMethodId)
        );
    }
);

Given(
    /^shipping rates for "(?<productId>[^"]*)" shipped to "(?<country>[^"]*)", "(?<region>[^"]*)", "(?<postalCode>[^"]*)", "(?<city>[^"]*)", "(?<street>[^"]*)" with "(?<methodId>[^"]*)" are:$/,
    function (
        this: World,
        productId: string,
        country: string,
        region: string,
        postalCode: string,
        city: string,
        street: string,
        methodId: string,
        shippingRates: DataTable,
    ): void {
        const shippingMethodId = new ShippingMethodId(methodId);

        this.app.shippingRateResolver.ratesPerShippingMethod[methodId] = shippingRates.hashes().map(
            (shippingRate) => ShippingRateTestBuilder.create()
                .withShippingMethodId(shippingMethodId)
                .withCode(shippingRate.code)
                .withTitle(shippingRate.title)
                .withPrice(parseFloat(shippingRate.price))
                .withDescription(shippingRate.description)
                .build()
        );
    }
);

When(
    /^user opens "(?<productId>[^"]*)" page$/,
    function (this: World, productId: string): void {
        this.selectedProductId = new ProductId(productId);
    }
);

Then(
    /^shipping rate "(?<methodId>[^"]*)" "(?<code>[^"]*)" with title "(?<methodTitle>[^"]*)" "(?<rateTitle>[^"]*)", price "(?<price>[^"]*)" and "(?<desc>[^"]*)" is displayed$/,
    async function (
        this: World,
        methodId: string,
        code: string,
        methodTitle: string,
        rateTitle: string,
        price: string,
        desc: string,
    ): Promise<void> {
        const minimalShippingRate = await this.app.productMinimalShippingRateQuery.execute(this.selectedProductId as ProductId);

        expect(minimalShippingRate).toEqual(new ShippingRateView(
            ShippingMethodTestBuilder.create()
                .withId(new ShippingMethodId(methodId))
                .withTitle(methodTitle)
                .build(),
            parseFloat(price),
            code,
            rateTitle,
            desc,
        ))
    }
);

When(
    /^user opens all shipping rates for "(?<productId>[^"]*)"$/,
    function (this: World, productId: string): void {
        this.selectedProductId = new ProductId(productId);
    }
);

Then(
    /^shipping rates are displayed:$/,
    async function (this: World, shippingRates: DataTable): Promise<void> {
        const result = await this.app.productShippingRatesQuery.execute(this.selectedProductId as ProductId);
        const expectedResult = shippingRates.hashes().map((shippingRate) => new ShippingRateView(
            ShippingMethodTestBuilder.create()
                .withId(new ShippingMethodId(shippingRate.shipping_method_id))
                .withTitle(shippingRate.shipping_method_title)
                .build(),
            parseFloat(shippingRate.price),
            shippingRate.shipping_rate_code,
            shippingRate.shipping_rate_title,
            shippingRate.description,
        ));

        let i = 0;
        for await (const resultItem of result) {
            expect(resultItem).toEqual(expectedResult[i++]);
        }
    }
);